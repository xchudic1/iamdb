# Movie Catalogue Web Application

## Overview

This web application serves as a catalogue of movies across several genres. Users can explore in collection of films, each has with a title, description, director, cast, and engaging images. Films are categorized into genres, allowing for easy navigation and discovery according to user preference.

## Features

- **Extensive Movie Properties**: Our catalogue provides information on each movie, including the title, description, director, cast and images from the scenes.

- **Genre Categorization**: Movies are organized into their genre, enabling users to browse and discover films that match their requests.

- **Movie Ratings**: Users have the opportunity to rate movies and write their review. These ratings help other users in selecting films that match their preferences.

- **Recommendations**: One of the standout features of our application is the ability to discover similar movies. Users can select a movie to see a list of similar films or those that are popular among viewers with similar tastes.(? im not sure about our rec algorithm)

- **Administrator Control**: Only administrators have the authority to create, update, or remove movies from the catalogue, ensuring that the database is well-maintained and up-to-date.

## Technologies Used

- Docker
- Spring Boot
- PostgreSQL
- MapStruct
- Grafana
- Prometheus

## Team Members

- Lukas Chudicek (team lead)
- Daniel Nemec
- Mario Straus
- Ecenur Sezer

## Modules

The information about each specific module can be found in the README.md file of each module.

- [Recommendation Microservice](./movie-recommender-microservice/README.md)
- [Movie Microservice](./movie-microservice/README.md)
- [Rating Microservice](./rating-microservice/README.md)
- [User Microservice](./user-microservice/README.md)

## Diagram

[The class diagram](./documentation/classdiagram.puml)

[The use case diagram](./documentation/usecase.puml)

Or rendered:

![Class diagram](./documentation/classdiagram.svg)

![Use case diagram](./documentation/usecase.svg)

## Running, Testing

Project can be built, ran and tested from the root directory.

Before doing so, read [developer readme](documentation/dev-documentation.md) to properly set up the environment.

Then, to test the project, run the following command:

```shell
mvn clean test
```

To run the project, execute the following command:

```shell
mvn clean install -DskipTests && \
docker-compose up -d
```

- note that you might need to clear the old images (if any present)

To observe the metrics of the system after the docker containers are up, please visit [Grafana UI](https://localhost:3000) with default credentials _admin:admin_.

A [screenshot](documentation/grafana-dashboard.png) of grafana board is also provided in documentation.
