# User Microservice

This microservice is responsible for tracking the information about users of the `iamdb` system.

Swagger ui available [here](http://localhost:8003/api/v1/swagger-ui/index.html#/).

The user microservice endpoints work in [localhost:8003/api/v1/users](http://localhost:8003/api/v1/users).

## Available Endpoints

### `POST /api/v1/users`

Creates a new user.

### `GET /api/v1/users`

Returns a list of all users in the system.

### `GET /api/v1/users/{id}`

Returns user with the specified ID.

### `PUT /api/v1/users/{id}`

Updates user with the specified ID. The ID cannot be changed, other fields can.

### `DELETE /api/v1/users/{id}`

Deletes user with the specified ID.

## clearing & seeding the database

clearing the database (change credentials, port, filepath if needed):

```shell
docker cp ./sql/clear_user_database.sql user-postgres:/ && \             
docker exec -it user-postgres psql -U user_user -d user_database -p 5433 -a -f ./clear_user_database.sql
```

seeding (change credentials, port, filepath if needed):

```shell
docker cp ./sql/seed_user_database.sql user-postgres:/ && \             
docker exec -it user-postgres psql -U user_user -d user_database -p 5433 -a -f ./seed_user_database.sql
```
