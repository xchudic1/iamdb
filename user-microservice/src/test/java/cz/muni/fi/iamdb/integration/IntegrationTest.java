package cz.muni.fi.iamdb.integration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
@Rollback
public class IntegrationTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    @Transactional
    void testCreate() throws Exception {
        String userJson = """
                {
                    "name": "alice",
                    "email": "valid@mail.com",
                    "role": "USER"
                }
                """;
        mockMvc.perform(post("/users")
                        .contentType("application/json")
                        .content(userJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    @Transactional
    void testFindAll() throws Exception {
        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @Transactional
    void testFindById() throws Exception {
        // setup
        String userJson = """
                {
                    "name": "alice",
                    "email": "valid@mail.com",
                    "role": "USER"
                }
                """;


        String jsonResponse = mockMvc.perform(post("/users")
                        .contentType("application/json")
                        .content(userJson)).andReturn().getResponse().getContentAsString();
        JsonNode jsonNode = new ObjectMapper().readTree(jsonResponse);
        String insertedUuidButString = jsonNode.get("id").asText();

        // get & assert
        mockMvc.perform(get("/users/" + insertedUuidButString))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(insertedUuidButString));
    }

    @Test
    @Transactional
    void testDelete() throws Exception {
        // setup
        String userJson = """
                {
                    "name": "alice",
                    "email": "valid@mail.com",
                    "role": "USER"
                }
                """;


        String jsonResponse = mockMvc.perform(post("/users")
                .contentType("application/json")
                .content(userJson)).andReturn().getResponse().getContentAsString();
        JsonNode jsonNode = new ObjectMapper().readTree(jsonResponse);
        String insertedUuidButString = jsonNode.get("id").asText();

        // act
        mockMvc.perform(delete("/users/" + insertedUuidButString))
                .andExpect(status().isNoContent());

        mockMvc.perform(delete("/users/" + insertedUuidButString))
                .andExpect(status().isNoContent()); // delete idempotent
    }

    @Test
    @Transactional
    void testDeleteNonexistent() throws Exception {
        mockMvc.perform(delete("/users/00000000-0000-0000-0000-000000000000"))
                .andExpect(status().isNoContent()); // delete idempotent
    }

    @Test
    void testCreateInvalid() throws Exception {
        String userJson = """
                {
                    "name": "alice",
                    "role": "USER"
                }
                """;

        mockMvc.perform(post("/users")
                .contentType("application/json")
                .content(userJson))
                // FUCK WHOLE JPA
                .andExpect(status().isInternalServerError());
    }
}
