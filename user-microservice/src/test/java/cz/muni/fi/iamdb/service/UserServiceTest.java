package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.User;
import cz.muni.fi.iamdb.data.repository.UserRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void create_userCreated_returnsUser() {
        // Arrange
        User newUser = new User(null, "username", "email@ecos.com", "role");
        Mockito.when(userRepository.save(newUser)).thenReturn(newUser);

        // Act
        User createdUser = userService.create(newUser);

        // Assert
        assertThat(createdUser).isEqualTo(newUser);
    }

    @Test
    void findById_userFound_returnsUser() {
        // Arrange
        UUID id = UUID.randomUUID();
        User user = new User(id, "username", "email@maiiiillll.com", "role");
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));

        // Act
        User foundUser = userService.findById(id);

        // Assert
        assertThat(foundUser).isEqualTo(user);
    }

    @Test
    void findById_userNotFound_throwsResourceNotFoundException() {
        // Arrange
        UUID id = UUID.randomUUID();
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.empty());

        // Assert
        assertThrows(ResourceNotFoundException.class, () -> userService.findById(id));
    }

    @Test
    void findAll_returnsListOfUsers() {
        // Arrange
        List<User> users = List.of(
                new User(UUID.randomUUID(), "username1", "email1@example.com", "role1"),
                new User(UUID.randomUUID(), "username2", "email2@example.com", "role2")
        );
        Mockito.when(userRepository.findAll()).thenReturn(users);

        // Act
        List<User> foundUsers = userService.findAll();

        // Assert
        assertThat(foundUsers).hasSameElementsAs(users);
    }

    @Test
    void update_userExists_updatesUser() {
        // Arrange
        UUID id = UUID.randomUUID();
        User user = new User(id, "username", "email@example.com", "role");
        Mockito.when(userRepository.save(user)).thenReturn(user);

        // Act & Assert (No exception should be thrown)
        userService.update(user);
    }

    @Test
    void update_userDoesNotExist_throwsResourceNotFoundException() {
        // Arrange
        UUID id = UUID.randomUUID();
        User user = new User(id, "username", "email@mailmail.com", "role");
        Mockito.when(userRepository.save(user)).thenReturn(null);

        // Assert
        assertThrows(ResourceNotFoundException.class, () -> userService.update(user));
    }

    @Test
    void delete_userExists_deletesUser() {
        // Arrange
        UUID id = UUID.randomUUID();
        userRepository.deleteById(id);

        // Act & Assert (No exception should be thrown)
        userService.delete(id);
    }

    @Test
    void delete_userDoesNotExist_doesNotThrow() {
        // Arrange
        UUID id = UUID.randomUUID();
        userRepository.deleteById(id);

        // delete idempotent; should succeed (not throw exception) even if the user does not exist
        assertDoesNotThrow(() -> userService.delete(id));
    }
}
