package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.UserBasicDto;
import cz.muni.fi.iamdb.facade.UserFacade;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = UserRestController.class)
class UserRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserFacade userFacade;

    @Test
    void create_whenCalled_returnsCreatedStatus() throws Exception {
        mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"username\",\"email\":\"ece@bored.com\"}")) // Simplified user JSON object.
                .andExpect(status().isCreated());
    }

    @Test
    void findById_whenUserExists_returnsUser() throws Exception {
        UUID id = UUID.randomUUID();
        UserBasicDto userDto = new UserBasicDto(id, "username", "m@m.com", "USER");

        Mockito.when(userFacade.findById(id)).thenReturn(userDto);

        mockMvc.perform(get("/users/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id.toString()))
                .andExpect(jsonPath("$.name").value("username"))
                .andExpect(jsonPath("$.email").value("m@m.com"));
    }

    @Test
    void findAll_whenCalled_returnsListOfUsers() throws Exception {
        List<UserBasicDto> users = List.of(
                new UserBasicDto(UUID.randomUUID(), "user1vojtik", "user1@mail.com", "USER"),
                new UserBasicDto(UUID.randomUUID(), "user2ece", "user2@mail.com", "ADMIN"));

        Mockito.when(userFacade.findAll()).thenReturn(users);

        mockMvc.perform(get("/users")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    void update_whenCalled_returnsOkStatus() throws Exception {
        UUID id = UUID.randomUUID();

        mockMvc.perform(put("/users/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"" + id + "\", \"name\":\"newus\",\"email\":\"newgm@example.com\"}")) // Simplified user JSON object.
                .andExpect(status().isOk());
    }

    @Test
    void delete_whenCalled_returnsNoContentStatus() throws Exception {
        UUID id = UUID.randomUUID();

        mockMvc.perform(delete("/users/{id}", id))
                .andExpect(status().isNoContent());
    }
}
