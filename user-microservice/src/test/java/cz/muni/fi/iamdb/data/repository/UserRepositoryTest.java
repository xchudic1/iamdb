package cz.muni.fi.iamdb.data.repository;

import cz.muni.fi.iamdb.data.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    private User mock_user1;
    private User mock_user2;

    @BeforeEach
    void setUp() {
        mock_user1 = new User(null, "ece", "ece@example.com", "USER");
        mock_user2 = new User(null, "admin", "woo@example.com", "ADMIN");
        userRepository.save(mock_user1);
        userRepository.save(mock_user2);
    }

    @Test
    void findById_ShouldReturnUser_WhenUserExists() {
        var result = userRepository.findById(mock_user1.getId());

        assertTrue(result.isPresent());
        assertEquals(mock_user1.getId(), result.get().getId());
    }

    @Test
    void findAll_ShouldReturnAllUsers() {
        List<User> users = userRepository.findAll();
        assertFalse(users.isEmpty());
        assertEquals(users.size(), 2); // ~~There might be more users from other tests~~ not true
    }

    @Test
    void update_ShouldUpdateUser_WhenUserExists() {
        String updatedEmail = "ecenin_updated@example.com";
        User updatedUser = new User(mock_user1.getId(), mock_user1.getName(), updatedEmail, mock_user1.getRole());
        assertNotEquals(userRepository.save(updatedUser), null);

        var result = userRepository.findById(mock_user1.getId());
        assertTrue(result.isPresent());
        assertEquals(updatedEmail, result.get().getEmail());
    }

    // apparently the constraints of the entity are not checked
//    @Test
//    void create_ShouldFail_WhenEmailIsInvalid() {
//        User invalidUser = new User(null, "invalid", "invalid_email", "USER");
//        assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(invalidUser));
//    }

    @Test
    void delete_ShouldRemoveUser_WhenUserExists() {
        User newUser = new User(null, "ece", "uniquemail@example.com", "USER");
        userRepository.save(newUser);

        var result = userRepository.findById(newUser.getId());
        assertTrue(result.isPresent());

        // delete the user
        userRepository.deleteById(newUser.getId());

        // check if the user is deleted
        result = userRepository.findById(newUser.getId());
        assertTrue(result.isEmpty());
    }
}
