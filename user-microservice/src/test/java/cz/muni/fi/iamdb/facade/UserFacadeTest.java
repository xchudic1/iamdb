package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.UserBasicDto;
import cz.muni.fi.iamdb.data.model.User;
import cz.muni.fi.iamdb.mappers.UserMapper;
import cz.muni.fi.iamdb.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class UserFacadeTest {

    @Mock
    private UserService userService;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserFacade userFacade;

    @BeforeEach
    void setUp() {

    }

    @Test
    void create_whenCalled_mapsAndReturnsUserDto() {
        // Arrange
        User user = new User(UUID.randomUUID(), "username", "ecenur@example.com", "ADMIN");
        UserBasicDto userDto = new UserBasicDto(user.getId(), user.getName(), user.getEmail(), user.getRole());

        Mockito.when(userService.create(any(User.class))).thenReturn(user);
        Mockito.when(userMapper.mapToDto(any(User.class))).thenReturn(userDto);

        // Act
        UserBasicDto result = userFacade.create(user);

        // Assert
        assertThat(result).isEqualTo(userDto);
    }

    @Test
    void findById_whenFound_mapsAndReturnsUserDto() {
        // Arrange
        UUID id = UUID.randomUUID();
        User user = new User(id, "username", "email@gmail.com", "USER");
        UserBasicDto userDto = new UserBasicDto(user.getId(), user.getName(), user.getEmail(), user.getRole());

        Mockito.when(userService.findById(id)).thenReturn(user);
        Mockito.when(userMapper.mapToDto(any(User.class))).thenReturn(userDto);

        // Act
        UserBasicDto result = userFacade.findById(id);

        // Assert
        assertThat(result).isEqualTo(userDto);
    }

    @Test
    void findAll_whenCalled_mapsAndReturnsListOfUserDtos() {
        // Arrange
        List<User> users = List.of(
                new User(UUID.randomUUID(), "user1ece", "user1@example.com", "USER"),
                new User(UUID.randomUUID(), "user2ece", "user2@example.com",  "ADMIN")
        );
        List<UserBasicDto> userDtos = users.stream()
                .map(user -> new UserBasicDto(user.getId(), user.getName(), user.getEmail(), user.getRole()))
                .toList();

        Mockito.when(userService.findAll()).thenReturn(users);
        Mockito.when(userMapper.mapToList(any(List.class))).thenReturn(userDtos);

        // Act
        List<UserBasicDto> result = userFacade.findAll();

        // Assert
        assertThat(result).hasSameElementsAs(userDtos);
    }

    @Test
    void update_whenCalled_updatesUser() {
        // Arrange
        UUID id = UUID.randomUUID();
        User userToUpdate = new User(id, "newusernameece", "updatedmail31@example.com", "USER");

        // Act
        userFacade.update(userToUpdate);

        // Assert
        Mockito.verify(userService).update(userToUpdate);
    }

    @Test
    void delete_whenCalled_deletesUser() {
        // Arrange
        UUID id = UUID.randomUUID();

        // Act
        userFacade.delete(id);

        // Assert
        Mockito.verify(userService).delete(id);
    }


}
