package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.UserBasicDto;
import cz.muni.fi.iamdb.data.model.User;
import cz.muni.fi.iamdb.mappers.UserMapper;
import cz.muni.fi.iamdb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class UserFacade {
    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public UserFacade(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public UserBasicDto create(User user) {
        return userMapper.mapToDto(userService.create(user));
    }

    public UserBasicDto findById(UUID id) {
        return userMapper.mapToDto(userService.findById(id));
    }

    public List<UserBasicDto> findAll() {
        return userMapper.mapToList(userService.findAll());
    }

    public void update(User user) {
        userService.update(user);
    }

    public void delete(UUID id) {
        userService.delete(id);
    }
}
