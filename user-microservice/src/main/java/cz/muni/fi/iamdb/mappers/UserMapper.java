package cz.muni.fi.iamdb.mappers;

import cz.muni.fi.iamdb.api.UserBasicDto;
import cz.muni.fi.iamdb.data.model.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserBasicDto mapToDto(User user);
    List<UserBasicDto> mapToList(List<User> users);
}
