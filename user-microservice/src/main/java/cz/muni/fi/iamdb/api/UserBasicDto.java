package cz.muni.fi.iamdb.api;

import jakarta.validation.constraints.NotEmpty;
import org.springframework.lang.NonNull;

import java.util.UUID;

/**
 * User basic view DTO
 * @param id UUID
 * @param name String
 * @param email String
 * @param role String
 */
public record UserBasicDto(
        @NonNull
        UUID id,
        @NotEmpty
        String name,
        @NotEmpty
        String email,
        @NotEmpty
        String role
) {}
