package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.User;
import cz.muni.fi.iamdb.data.repository.UserRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@Service
public class UserService {
    private final UserRepository userRepository;

    Logger logger = Logger.getLogger(UserService.class.getName());

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional /* (readOnly = false) */
    public User create(User user) {
        if (user.getId() != null) {
            // bych blil docela
            throw new IllegalArgumentException("User id should be null on insert.");
        }
        try {
            return userRepository.save(user);
        } catch (RuntimeException e) {
            logger.severe("Error during user creation: " + e);
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public User findById(UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User with id: " + id + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Transactional /* (readOnly = false) */
    public void update(User user) {

        if (user.getId() == null) {
            throw new IllegalArgumentException("User id should not be null on update.");
        }

        User status = userRepository.save(user);

        // what the fuck how do i detect if it failed? wym `condition is always false`?
        if (status == null) {
            // exceptions for control flow lets go
            throw new ResourceNotFoundException("User with id: " + user.getId() + " was not updated.");
        }
    }

    @Transactional /* (readOnly = false) */
    public void delete(UUID id) {
        userRepository.deleteById(id);
    }
}
