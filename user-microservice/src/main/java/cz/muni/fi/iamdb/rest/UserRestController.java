package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.UserBasicDto;
import cz.muni.fi.iamdb.data.model.User;
import cz.muni.fi.iamdb.facade.UserFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/users")
public class UserRestController {
    private final UserFacade userFacade;

    Logger logger = Logger.getLogger(UserRestController.class.getName());

    @Autowired
    public UserRestController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @PostMapping(path = "")
    @Operation(summary = "Create user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "User already exists")
    })
    public ResponseEntity<UserBasicDto> create(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "User to be created") @RequestBody User user) {

        logger.info("Creating user: " + user.toString());

        try {
            var resp = ResponseEntity.status(201).body(userFacade.create(user));
            logger.info("User created: " + user.toString());
            return resp;
        } catch (Exception e) {
            logger.severe("Error during user creation: " + e.toString());
            throw e;
        }
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User found"),
            @ApiResponse(responseCode = "404", description = "User not found")
    })
    public ResponseEntity<UserBasicDto> findById(
            @Parameter(description = "ID of user to be searched") @PathVariable UUID id) {
        return ResponseEntity.ok(userFacade.findById(id));
    }


    @GetMapping(path = "")
    @Operation(summary = "Find all users")
    @ApiResponse(responseCode = "200", description = "List of users found")
    public ResponseEntity<List<UserBasicDto>> findAll() {
        return ResponseEntity.ok(userFacade.findAll());
    }

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "User not found")
    })
    public ResponseEntity<Void> update(
            @Parameter(description = "ID of user to be updated") @PathVariable UUID id,
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "User data to be updated") @RequestBody User user) {
        if (!id.equals(user.getId())) {
            return ResponseEntity.badRequest().build();
        }

        userFacade.update(user);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User deleted"),
            @ApiResponse(responseCode = "404", description = "User not found")
    })
    public ResponseEntity<Void> delete(
            @Parameter(description = "ID of user to be deleted") @PathVariable UUID id) {
        userFacade.delete(id);
        return ResponseEntity.noContent().build();
    }
}
