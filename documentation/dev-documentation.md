# project setup

- set up you local `.env` file (eg by running)

```shell
cp .env.example .env
```

- those variables then need to populate the environment variables of your current terminal session

```shell
export $(grep -v '^#' .env | xargs)
```

- or powershell equivalent (rip)

```powershell
# todo tbh no idea; pls find the way, test it, push here
```

- all the images of the java services need to be rebuilt
  - ensure there are no old images of the services in your local docker registry
  - then run the following

```shell
mvn clean install -DskipTests
```

- make sure your docker daemon is running
- run the following (or equivalent for your specific system) to clear out any ports that might be in use
  - or change your local `.env` to specify a different port

```shell
docker stop $(docker ps -a -q) && sudo systemctl stop postgresql
```

- or powershell equivalent (rip)

```powershell
docker ps -q | ForEach-Object { docker stop $_ } ; Stop-Service -Name postgresql
```

- run the following to run the containers

```shell
docker-compose up -d
```

- ensure everything works by running the following

```shell
psql -h localhost -p 5431 -U movie_user -d movie_database
```

- (specific <user>, <port>, <database> and <password> are in the `.env` file)
