# Movie Recommender Microservice


## Getting Started

To get the microservice running on your local machine for development and testing purposes, follow these steps:

```bash
mvn clean install
java -jar .\target\movie-recommender-microservice-1.0.0.jar
```
## Movie recommender API endpoints

This section details the available API endpoints 

The movie recommender microservice endpoints work in [localhost:8000/api/v1/movie-recommender](localhost:8000/api/v1/movie-recommender).

for the Movie recommender managing in Movie Recommender Microservice,
including the 
expected request bodies and the responses for each operation.

### Get Best Rated Movie In Genre

### `GET /api/v1/movie-recommender/best-rated-in-genre/{id}`

Returns best rated movie in given genre.

### Get Recommended Movies

### `GET /api/v1/movie-recommender/recommend/{id}`

Returns recommended movie according to given movie id. 

### `GET /api/v1/movie-recommender/recommended-movies-according-to-rating-in-same-genre/{id}`

Returns recommended movies according to rating in same genre.
## Testing

You can use swagger with url <http://localhost:8000/api/v1/swagger-ui/index.html#/> to test the endpoints.

- (your host, port may be different depending on your env)
