
package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.GenreDto;
import cz.muni.fi.iamdb.api.MovieBasicViewDto;
import cz.muni.fi.iamdb.service.MovieRecommenderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class MovieRecommenderRestControllerTest {

    @Mock
    private MovieRecommenderService movieRecommenderService;

    @InjectMocks
    private MovieRecommenderController movieRecommenderController;

    @Test
    void recommendMovies_noMovies_returnsEmptyList() {
        // setup
        UUID id = UUID.randomUUID();
        Mockito.when(movieRecommenderService.getRecommendedMovies(id)).thenReturn(Collections.emptyList());

        // act
        var response = movieRecommenderController.recommendMovies(id);

        // assert
        assertThat(response).isEmpty();
    }

    @Test
    void recommendMovies_singleMovie_returnsThatMovie() {
        // setup
        UUID id = UUID.randomUUID();
        GenreDto genre = new GenreDto();
        genre.setName("Action");
        var movie = new MovieBasicViewDto(id, "title", 1, 1, new GenreDto[] {genre});
        Mockito.when(movieRecommenderService.getRecommendedMovies(id)).thenReturn(Collections.singletonList(movie));

        // act
        var response = movieRecommenderController.recommendMovies(id);

        // assert
        assertThat(response).containsExactly(movie);
    }

    @Test
    void getBestRatedMovieInGenre_singleMovie_returnsThatMovie() {
        // setup
        UUID id = UUID.randomUUID();
        GenreDto genre = new GenreDto();
        genre.setName("Action");
        var movie = new MovieBasicViewDto(id, "title", 1, 1, new GenreDto[] {genre});
        Mockito.when(movieRecommenderService.getBestRatedMovieInGenre(id)).thenReturn(Optional.of(movie));

        // act
        var response = movieRecommenderController.getBestRatedMovieInGenre(id);

        // assert
        assertThat(response).contains(movie);
    }

    @Test
    void recommendMovies_multipleMovies_returnsAllMovies() {
        // setup

        UUID id_1 = UUID.randomUUID();
        GenreDto genre_1 = new GenreDto();
        genre_1.setName("Action");
        var movie1 = new MovieBasicViewDto(id_1, "title1", 1, 1, new GenreDto[] {genre_1});

        UUID id_2 = UUID.randomUUID();
        GenreDto genre_2 = new GenreDto();
        genre_2.setName("Action");
        var movie2 = new MovieBasicViewDto(id_2, "title2", 2, 2, new GenreDto[] {genre_2});
        Mockito.when(movieRecommenderService.getRecommendedMovies(id_1)).thenReturn(List.of(movie1, movie2));

        // act
        var response = movieRecommenderController.recommendMovies(id_1);

        // assert
        assertThat(response).containsExactly(movie1, movie2);
    }

    @Test
    void getBestRatedMovieInGenre_noMovies_returnsEmptyOptional() {
        // setup
        UUID id = UUID.randomUUID();
        Mockito.when(movieRecommenderService.getBestRatedMovieInGenre(id)).thenReturn(Optional.empty());

        // act
        var response = movieRecommenderController.getBestRatedMovieInGenre(id);

        // assert
        assertThat(response).isEmpty();
    }

    @Test
    void getRecommendedMoviesAccordingToRatingInSameGenre_noMovies_returnsEmptyList() {
        // setup
        UUID id = UUID.randomUUID();
        Mockito.when(movieRecommenderService.getRecommendedMoviesAccordingToRatingInSameGenre(id)).thenReturn(Collections.emptyList());

        // act
        var response = movieRecommenderController.getRecommendedMoviesAccordingToRatingInSameGenre(id);

        // assert
        assertThat(response).isEmpty();
    }

    @Test
    void getRecommendedMoviesAccordingToRatingInSameGenre_singleMovie_returnsThatMovie() {
        // setup
        UUID id = UUID.randomUUID();
        GenreDto genre = new GenreDto();
        genre.setName("Action");
        var movie = new MovieBasicViewDto(id, "title", 1, 1, new GenreDto[] {genre});
        Mockito.when(movieRecommenderService.getRecommendedMoviesAccordingToRatingInSameGenre(id)).thenReturn(Collections.singletonList(movie));

        // act
        var response = movieRecommenderController.getRecommendedMoviesAccordingToRatingInSameGenre(id);

        // assert
        assertThat(response).containsExactly(movie);
    }
}
