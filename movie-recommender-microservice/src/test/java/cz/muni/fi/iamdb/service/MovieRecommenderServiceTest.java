package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.api.RatingDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import cz.muni.fi.iamdb.api.GenreDto;
import cz.muni.fi.iamdb.api.MovieBasicViewDto;
import java.util.UUID;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class MovieRecommenderServiceTest {

    @Value("${app.urls.movies}")
    private String MOVIES_URL;

    @Value("${app.urls.ratings}")
    private String RATINGS_URL;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private MovieRecommenderService movieRecommenderService;

    @Test
    void getRecommendedMovies() {
        // setup
        UUID id = UUID.randomUUID();
        GenreDto genre = new GenreDto();
        genre.setName("Action");
        var movie = new MovieBasicViewDto(id, "title", 1, 1, new GenreDto[] {genre});

        UUID id_2 = UUID.randomUUID();
        GenreDto genre_2 = new GenreDto();
        genre_2.setName("Action");
        var movie_2 = new MovieBasicViewDto(id_2, "title2", 2, 2, new GenreDto[] {genre_2});

        Mockito.when(restTemplate.getForEntity(MOVIES_URL, MovieBasicViewDto[].class)).thenReturn(ResponseEntity.ok(new MovieBasicViewDto[] {movie, movie_2}));

        // act
        var response = movieRecommenderService.getRecommendedMovies(id);

        // assert
        assertThat(response).containsExactly(movie_2);
    }

    @Test
    void getRecommendedMovies_noMovies() {
        // setup
        UUID id = UUID.randomUUID();
        Mockito.when(restTemplate.getForEntity(MOVIES_URL, MovieBasicViewDto[].class)).thenReturn(ResponseEntity.ok(new MovieBasicViewDto[0]));

        // act
        var response = movieRecommenderService.getRecommendedMovies(id);

        // assert
        assertThat(response).isEmpty();
    }

    @Test
    void getRecommendedMovies_noGivenMovie() {
        // setup
        UUID id = UUID.randomUUID();
        GenreDto genre = new GenreDto();
        genre.setName("Action");
        var movie = new MovieBasicViewDto(UUID.randomUUID(), "title", 1, 1, new GenreDto[] {genre});

        Mockito.when(restTemplate.getForEntity(MOVIES_URL, MovieBasicViewDto[].class)).thenReturn(ResponseEntity.ok(new MovieBasicViewDto[] {movie}));

        // act
        var response = movieRecommenderService.getRecommendedMovies(id);

        // assert
        assertThat(response).isEmpty();
    }


    @Test
    void getRecommendedMoviesAccordingToRatingInSameGenre_noGivenMovie() {
        // setup
        UUID id = UUID.randomUUID();
        Mockito.when(restTemplate.getForEntity(MOVIES_URL + "/" + id, MovieBasicViewDto.class)).thenReturn(ResponseEntity.ok(null));

        // act
        var response = movieRecommenderService.getRecommendedMoviesAccordingToRatingInSameGenre(id);

        // assert
        assertThat(response).isEmpty();
    }

    @Test
    void getRecommendedMoviesAccordingToRatingInSameGenre_noMoviesWithGivenGenre() {
        // setup
        UUID id = UUID.randomUUID();
        GenreDto genre = new GenreDto();
        genre.setId(UUID.randomUUID());
        genre.setName("Action");
        var movie = new MovieBasicViewDto(id, "title", 1, 1, new GenreDto[] {genre});

        Mockito.when(restTemplate.getForEntity(MOVIES_URL + "/" + id, MovieBasicViewDto.class)).thenReturn(ResponseEntity.ok(movie));
        Mockito.when(restTemplate.getForEntity(MOVIES_URL + "?genreId=" + genre.getId(), MovieBasicViewDto[].class)).thenReturn(ResponseEntity.ok(new MovieBasicViewDto[0]));

        // act
        var response = movieRecommenderService.getRecommendedMoviesAccordingToRatingInSameGenre(id);

        // assert
        assertThat(response).isEmpty();
    }

    @Test
    void getRecommendedMoviesAccordingToRatingInSameGenre() {
        // setup
        UUID id = UUID.randomUUID();
        GenreDto genre = new GenreDto();
        genre.setName("Action");
        var movie = new MovieBasicViewDto(id, "title", 1, 1, new GenreDto[] {genre});

        UUID id_2 = UUID.randomUUID();
        GenreDto genre_2 = new GenreDto();
        genre_2.setName("Action");
        var movie_2 = new MovieBasicViewDto(id_2, "title2", 2, 2, new GenreDto[] {genre_2});

        RatingDto rating = new RatingDto();
        rating.setScore(5.0);
        rating.setMovieId(id_2);

        Mockito.when(restTemplate.getForEntity(MOVIES_URL + "/" + id, MovieBasicViewDto.class)).thenReturn(ResponseEntity.ok(movie));
        Mockito.when(restTemplate.getForEntity(MOVIES_URL + "?genreId=" + genre.getId(), MovieBasicViewDto[].class)).thenReturn(ResponseEntity.ok(new MovieBasicViewDto[] {movie_2}));
        Mockito.when(restTemplate.getForEntity(RATINGS_URL + "/movie/" + id_2, RatingDto[].class))
                .thenReturn(ResponseEntity.ok(new RatingDto[] {rating}));
        // act
        var response = movieRecommenderService.getRecommendedMoviesAccordingToRatingInSameGenre(id);

        // assert
        assertThat(response).containsExactly(movie_2);
    }

}


