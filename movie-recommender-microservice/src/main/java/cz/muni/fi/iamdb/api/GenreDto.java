package cz.muni.fi.iamdb.api;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.UUID;

public class GenreDto {
        @NotBlank(message = "Description must not be blank")
        @NotNull
        private String name;
        private UUID id;

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }


        public UUID getId() {
                return id;
        }

        public void setId(UUID id) {
                this.id = id;
        }

}
