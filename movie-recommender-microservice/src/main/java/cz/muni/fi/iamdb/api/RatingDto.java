package cz.muni.fi.iamdb.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class RatingDto {

    private UUID id;
    @JsonProperty("movie_id")
    private UUID movieId;
    @JsonProperty("user_id")
    private UUID userId;
    private Double score;
    private String review;

    public RatingDto(UUID id, UUID movieId, UUID userId, Double score, String review) {
        this.id = id;
        this.movieId = movieId;
        this.userId = userId;
        this.score = score;
        this.review = review;
    }

    public RatingDto() {

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getMovieId() {
        return movieId;
    }

    public void setMovieId(UUID movieId) {
        this.movieId = movieId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        if (score < 1.0 || score > 10.0) {
            throw new IllegalArgumentException("Score must be a number between 1 and 10.");
        }
        this.score = score;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) {
            return true;
        }
        if (!(o instanceof RatingDto ratingDto)) {
            return false;
        }
        return getId().equals(ratingDto.getId());
    }
    @Override
    public String toString() {
        return "RatingDto{" +
                "id=" + id +
                ", movieId=" + movieId +
                ", userId=" + userId +
                ", score=" + score +
                ", review='" + review + '\'' +
                '}';
    }
}
