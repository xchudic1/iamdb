package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.MovieBasicViewDto;
import cz.muni.fi.iamdb.service.MovieRecommenderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class MovieRecommenderController {
    private final MovieRecommenderService movieRecommenderService;

    @Autowired
    public MovieRecommenderController(MovieRecommenderService movieRecommenderService) {
        this.movieRecommenderService = movieRecommenderService;
    }

    @GetMapping("/recommend/{id}")
    @Operation(summary = "Find recommended movies according to the movie with the given id")
    @ApiResponse(responseCode = "200", description = "Movies recommended")
    public List<MovieBasicViewDto> recommendMovies(@PathVariable UUID id) {
        return movieRecommenderService.getRecommendedMovies(id);
    }

    @GetMapping("/best-rated-in-genre/{id}")
    @Operation(summary = "Find the best rated movie in the genre with the given id")
    @ApiResponse(responseCode = "200", description = "Movie found")
    public Optional<MovieBasicViewDto> getBestRatedMovieInGenre(@PathVariable UUID id) {
        return movieRecommenderService.getBestRatedMovieInGenre(id);
    }

    @GetMapping("/recommended-movies-according-to-rating-in-same-genre/{id}")
    @Operation(summary = "Find recommended movies according to the rating in the same genre as the movie with the given id")
    @ApiResponse(responseCode = "200", description = "Movie found")
    public List<MovieBasicViewDto> getRecommendedMoviesAccordingToRatingInSameGenre(@PathVariable UUID id) {
        return movieRecommenderService.getRecommendedMoviesAccordingToRatingInSameGenre(id);
    }
}
