package cz.muni.fi.iamdb.api;

import java.util.UUID;

public record MovieBasicViewDto (
        UUID id,
        String title,
        int duration,
        int yearOfRelease,
        GenreDto[] genres
) {
}
