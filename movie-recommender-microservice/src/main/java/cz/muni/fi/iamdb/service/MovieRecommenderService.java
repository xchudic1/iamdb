package cz.muni.fi.iamdb.service;


import cz.muni.fi.iamdb.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


import java.util.*;
import java.util.stream.Collectors;

@Service
public class MovieRecommenderService {
    private final RestTemplate restTemplate;

    @Value("${app.urls.movies}")
    private String MOVIES_URL;
    @Value("${app.urls.ratings}")
    private String RATINGS_URL;

    @Autowired
    public MovieRecommenderService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    public List<MovieBasicViewDto> getRecommendedMovies(UUID movieId) {
        // this is how you get all the movies (which would be bad normally, but whatever)
        ResponseEntity<MovieBasicViewDto[]> allMoviesResponse = restTemplate.getForEntity(
                MOVIES_URL,
                MovieBasicViewDto[].class
        );

        var allMovies = allMoviesResponse.getBody();

        // if there are no movies, return empty list
        if (allMovies == null || allMovies.length == 0) {
            return Collections.emptyList();
        }

        var givenMovie = Arrays.stream(allMovies)
                .filter(movie -> Objects.equals(movie.id(), movieId))
                .findFirst()
                .orElse(null);

        if (givenMovie == null) {
            return Collections.emptyList();
        }

        List<MovieSimilarity> similarities = Arrays.stream(allMovies)
                .filter(movie -> !Objects.equals(movie.id(), movieId))
                .map(movie -> {
                    double similarity = calculateGenreSimilarity(givenMovie, movie);
                    return new MovieSimilarity(movie, similarity);
                }).sorted().toList();

        return similarities.stream()
                .limit(8)
                .map(similarity -> similarity.movie)
                .collect(Collectors.toList());
    }

    public List<MovieBasicViewDto> getRecommendedMoviesAccordingToRatingInSameGenre(UUID movieId) {
        ResponseEntity<MovieBasicViewDto> givenMoviesResponse = restTemplate.getForEntity(
                MOVIES_URL + "/" + movieId,
                MovieBasicViewDto.class
        );

        var givenMovie = givenMoviesResponse.getBody();
        if (givenMovie == null) {
            return Collections.emptyList();
        }

        var genreId = givenMovie.genres()[0].getId();

        ResponseEntity<MovieBasicViewDto[]> moviesWithGivenGenreResponse = restTemplate.getForEntity(
                MOVIES_URL + "?genreId=" + genreId,
                MovieBasicViewDto[].class
        );

        var moviesWithGivenGenre = moviesWithGivenGenreResponse.getBody();

        if (moviesWithGivenGenre == null || moviesWithGivenGenre.length == 0) {
            return Collections.emptyList();
        }

        Map<UUID, Double> movieRatings = Arrays.stream(moviesWithGivenGenre)
                .collect(Collectors.toMap(MovieBasicViewDto::id, this::calculateMovieRating));

        return Arrays.stream(moviesWithGivenGenre)
                .map(movie -> new MovieRating(movie, movieRatings.getOrDefault(movie.id(), 0.0)))
                .sorted()
                .limit(5)
                .map(movieRating -> movieRating.movie)
                .collect(Collectors.toList());

    }

    public Optional<MovieBasicViewDto> getBestRatedMovieInGenre(UUID genreId) {
        ResponseEntity<MovieBasicViewDto[]> moviesWithGivenGenreResponse = restTemplate.exchange(
                MOVIES_URL + "?genreId=" + genreId,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );

        var moviesWithGivenGenre = moviesWithGivenGenreResponse.getBody();

        if (moviesWithGivenGenre == null || moviesWithGivenGenre.length == 0) {
            return Optional.empty();
        }

        ResponseEntity<RatingDto[]> ratingsResponse = restTemplate.exchange(
                RATINGS_URL,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );

        var ratings = ratingsResponse.getBody();

        if (ratings == null || ratings.length == 0) {
            return Optional.empty();
        }


        Map<UUID, Double> movieRatings = Arrays.stream(ratings)
                .collect(Collectors.groupingBy(RatingDto::getMovieId, Collectors.averagingDouble(RatingDto::getScore)));

        List<MovieRating> movieRatingsWithGivenGenre = Arrays.stream(moviesWithGivenGenre)
                .map(movie -> new MovieRating(movie, movieRatings.getOrDefault(movie.id(), 0.0)))
                .sorted()
                .toList();

        if (movieRatingsWithGivenGenre.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(movieRatingsWithGivenGenre.get(0).movie);
    }

    private record MovieRating(MovieBasicViewDto movie, double rating) implements Comparable<MovieRating> {
        @Override
        public int compareTo(MovieRating other) {
            return Double.compare(other.rating, this.rating);
        }
    }


    private record MovieSimilarity(MovieBasicViewDto movie,
                                   double similarityScore) implements Comparable<MovieSimilarity> {
        @Override
        public int compareTo(MovieSimilarity other) {
            return Double.compare(other.similarityScore, this.similarityScore);
        }
    }

    private double calculateGenreSimilarity(MovieBasicViewDto givenMovie, MovieBasicViewDto otherMovie) {
        double intersection = Arrays.stream(givenMovie.genres())
                .filter(genre -> Arrays.stream(otherMovie.genres()).toList().contains(genre))
                .count();

        double union = givenMovie.genres().length + otherMovie.genres().length;
        if (union == 0) {
            return 0;
        }

        union -= intersection;
        return intersection / union;
    }

    private double calculateMovieRating(MovieBasicViewDto movie){
        ResponseEntity<RatingDto[]> ratingsResponse = restTemplate.getForEntity(
                RATINGS_URL + "/movie/" + movie.id(),
                RatingDto[].class
        );

        var ratings = ratingsResponse.getBody();

        if (ratings == null || ratings.length == 0) {
            return 0;
        }

        return Arrays.stream(ratings)
                .mapToDouble(RatingDto::getScore)
                .average()
                .orElse(0);
    }


}
