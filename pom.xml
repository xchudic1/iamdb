<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!-- parent project -->
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.2.4</version>
    </parent>

    <!-- this project -->
    <groupId>cz.muni.pa165</groupId>
    <artifactId>parent-project</artifactId>
    <version>0.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>iam-db</name>
    <description>
        iam-db
        The ultimate movie rating and suggesting service
        Semester project for PA165 - spring 2024
    </description>
    <organization>
        <name>Masaryk University</name>
        <url>https://www.muni.cz/</url>
    </organization>
    <inceptionYear>2024</inceptionYear>
    <developers>
        <developer>
            <name>Lukas Chudicek</name>
            <email>505943@mail.muni.cz</email>
            <url>https://www.linkedin.com/in/chudicek/</url>
            <organization>Masaryk University</organization>
            <organizationUrl>https://www.muni.cz</organizationUrl>
        </developer>
        <developer>
            <name>Daniel Němec</name>
            <email>493000@mail.muni.cz</email>
            <url>https://www.linkedin.com/in/daniel-n%C4%9Bmec-973096293/</url>
            <organization>Masaryk University</organization>
            <organizationUrl>https://www.muni.cz</organizationUrl>
        </developer>
        <developer>
            <name>Ecenur Sezer</name>
            <email>550864@mail.muni.cz</email>
            <organization>Masaryk University</organization>
            <organizationUrl>https://www.muni.cz</organizationUrl>
        </developer>
        <developer>
            <name>Mário Štraus</name>
            <email>493070@mail.muni.cz</email>
            <organization>Masaryk University</organization>
            <organizationUrl>https://www.muni.cz</organizationUrl>
        </developer>
    </developers>

    <!-- modules aggregation (commands are done in them too)  -->
    <modules>
        <module>client</module>
        <module>movie-recommender-microservice</module>
        <module>user-microservice</module>
        <module>movie-microservice</module>
        <module>rating-microservice</module>
    </modules>

    <!-- properties are inherited into children projects and can be used anywhere with ${property} -->
    <properties>
        <swagger-jakarta-version>2.2.20</swagger-jakarta-version>
        <version.mapstruct>1.5.5.Final</version.mapstruct>
        <version.failsafe>3.2.5</version.failsafe>
        <maven.compiler.source>21</maven.compiler.source>
        <maven.compiler.target>21</maven.compiler.target>
    </properties>

    <build>
        <!--
            Sets versions of plugins for all modules in one place.
            This does not include the listed plugins into modules,
            modules still must be declared in each module that uses them,
            just without <version>.
         -->
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.openapitools</groupId>
                    <artifactId>openapi-generator-maven-plugin</artifactId>
                    <version>7.4.0</version>
                </plugin>
                <plugin>
                    <groupId>org.springdoc</groupId>
                    <artifactId>springdoc-openapi-maven-plugin</artifactId>
                    <version>1.4</version>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <!-- JaCoCo Maven plugin -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.11</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>

    </build>

    <!--
        Set versions of dependencies for all modules in one place.
        This does not include the listed dependencies into modules,
        dependencies still must be declared in each module that uses them,
        just without <version>.
    -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springdoc</groupId>
                <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
                <version>2.3.0</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-text</artifactId>
                <version>1.11.0</version>
            </dependency>
            <dependency>
                <groupId>com.google.code.findbugs</groupId>
                <artifactId>jsr305</artifactId>
                <version>3.0.2</version>
            </dependency>
            <dependency>
                <groupId>org.openapitools</groupId>
                <artifactId>jackson-databind-nullable</artifactId>
                <version>0.2.6</version>
            </dependency>
            <dependency>
                <groupId>io.swagger</groupId>
                <artifactId>swagger-annotations</artifactId>
                <version>1.6.13</version>
            </dependency>
            <dependency>
                <groupId>javax.validation</groupId>
                <artifactId>validation-api</artifactId>
                <version>2.0.1.Final</version>
            </dependency>
            <dependency>
                <groupId>io.swagger.core.v3</groupId>
                <artifactId>swagger-models-jakarta</artifactId>
                <version>${swagger-jakarta-version}</version>
            </dependency>
            <dependency>
                <groupId>io.swagger.core.v3</groupId>
                <artifactId>swagger-annotations-jakarta</artifactId>
                <version>${swagger-jakarta-version}</version>
            </dependency>
            <dependency>
                <groupId>javax.annotation</groupId>
                <artifactId>javax.annotation-api</artifactId>
                <version>1.3.2</version>
            </dependency>
            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>42.7.3</version>
            </dependency>
            <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>2.2.224</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.micrometer</groupId>
                <artifactId>micrometer-registry-prometheus</artifactId>
                <version>1.9.3</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

</project>
