package cz.muni.fi.iamdb.data.repository;

import cz.muni.fi.iamdb.data.model.Comment;
import cz.muni.fi.iamdb.data.model.Rating;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest // Use DataJpaTest for integration testing with a real database
public class RatingRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RatingRepository ratingRepository;

    private Rating testRating1;
    private Rating testRating2;

    @BeforeEach
    void setUp() {
        testRating1 = new Rating(null, UUID.randomUUID(), UUID.randomUUID(), 8.0, "Very nice movie");
        testRating2 = new Rating(null, UUID.randomUUID(), UUID.randomUUID(), 3.6, "Not great not terrible");

        // Persist entities using TestEntityManager for a more realistic test setup
        testRating1 = entityManager.persistFlushFind(testRating1);
        testRating2 = entityManager.persistFlushFind(testRating2);

        Comment comment1 = new Comment(null, testRating1, UUID.randomUUID(), "Great movie!");
        Comment comment2 = new Comment(null, testRating1, UUID.randomUUID(), "Really enjoyed it!");

        entityManager.persistFlushFind(comment1);
        entityManager.persistFlushFind(comment2);
    }

    @AfterEach
    void tearDown() {
        entityManager.clear();
    }

    @Test
    void findById_ShouldReturnRating_WhenRatingExists() {
        Optional<Rating> result = ratingRepository.findById(testRating1.getId());
        assertTrue(result.isPresent());
        assertEquals(result.get().getId(), testRating1.getId());
    }

    @Test
    void findAll_ShouldReturnAllRatings() {
        List<Rating> result = ratingRepository.findAll();
        assertFalse(result.isEmpty());
        assertTrue(result.contains(testRating1));
        assertTrue(result.contains(testRating2));
    }

    @Test
    void update_ShouldUpdateRating_WhenRatingExists() {
        String updatedReview = "Updated review";
        testRating1.setReview(updatedReview);
        Rating updated = ratingRepository.save(testRating1); // Save to simulate update

        assertEquals(updatedReview, updated.getReview());
        assertTrue(ratingRepository.findById(testRating1.getId()).isPresent());
        assertEquals(updatedReview, ratingRepository.findById(testRating1.getId()).get().getReview());
    }

    @Test
    void delete_ShouldRemoveRating_WhenRatingExists() {
        ratingRepository.deleteById(testRating1.getId());
        assertFalse(ratingRepository.existsById(testRating1.getId()));
    }

    @Test
    void findAllByMovie_ShouldReturnAllRatingsByMovie() {
        List<Rating> result = ratingRepository.findDistinctByMovieId(testRating1.getMovieId());
        assertFalse(result.isEmpty());
        assertTrue(result.contains(testRating1));
    }

    @Test
    void findAllByUser_ShouldReturnAllRatingsByUser() {
        List<Rating> result = ratingRepository.findDistinctByUserId(testRating1.getUserId());
        assertFalse(result.isEmpty());
        assertTrue(result.contains(testRating1));
    }

    @Test
    void findAllByMovie_ShouldReturnEmptyList_WhenNoRatingsForMovie() {
        List<Rating> result = ratingRepository.findDistinctByMovieId(UUID.randomUUID());
        assertTrue(result.isEmpty());
    }

    @Test
    void findAllByUser_ShouldReturnEmptyList_WhenNoRatingsByUser() {
        List<Rating> result = ratingRepository.findDistinctByUserId(UUID.randomUUID());
        assertTrue(result.isEmpty());
    }
}
