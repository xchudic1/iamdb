package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.data.repository.RatingRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class RatingRepositoryIT {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RatingRepository ratingRepository;

    @Test
    void testCreateAndFindById() {
        UUID movieId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        Rating newRating = new Rating(null, movieId, userId, 9.5, "Excellent movie");
        newRating = entityManager.persistFlushFind(newRating);

        Rating foundRating = ratingRepository.findById(newRating.getId()).orElse(null);

        assertThat(foundRating).isNotNull();
        assertThat(foundRating.getScore()).isEqualTo(9.5);
        assertThat(foundRating.getReview()).isEqualTo("Excellent movie");
    }
}
