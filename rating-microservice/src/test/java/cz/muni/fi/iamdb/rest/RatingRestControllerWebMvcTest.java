package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.CommentDto;
import cz.muni.fi.iamdb.api.RatingDto;
import cz.muni.fi.iamdb.facade.RatingFacade;
import cz.muni.fi.iamdb.util.ObjectConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {RatingController.class})
@ActiveProfiles("test")
@AutoConfigureMockMvc(addFilters = false)
final class RatingRestControllerWebMvcTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RatingFacade ratingFacade;

    private RatingDto ratingDto;
    private CommentDto commentDto;
    private UUID ratingId;

    @BeforeEach
    void setUp() {
        ratingId = UUID.randomUUID();
        commentDto = new CommentDto(UUID.randomUUID(), ratingId, UUID.randomUUID(), "Great comment!");
        ratingDto = new RatingDto(ratingId, UUID.randomUUID(), UUID.randomUUID(), 8.6, "Test rating", List.of(commentDto));
    }

    @Test
    void findById_ratingFound_returnsResponseWithRating() throws Exception {
        Mockito.when(ratingFacade.findById(ratingDto.getId())).thenReturn(ratingDto);

        String responseJson = mockMvc.perform(get("/ratings/{id}", ratingDto.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        RatingDto response = ObjectConverter.convertJsonToObject(responseJson, RatingDto.class);

        assertThat(responseJson).isEqualTo("{\"id\":\"" + ratingDto.getId() + "\",\"score\":8.6,\"review\":\"Test rating\",\"comments\":[{\"id\":\"" + commentDto.getId() + "\",\"text\":\"Great comment!\",\"rating_id\":\"" + ratingId + "\",\"user_id\":\"" + commentDto.getUserId() + "\"}],\"movie_id\":\"" + ratingDto.getMovieId() + "\",\"user_id\":\"" + ratingDto.getUserId() + "\"}");
        assertThat(response).isEqualTo(ratingDto);
    }

    @Test
    void findAll_ratingsFound_returnsResponseWithRatings() throws Exception {
        Mockito.when(ratingFacade.findAll()).thenReturn(List.of(ratingDto));

        String responseJson = mockMvc.perform(get("/ratings")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        List<RatingDto> response = ObjectConverter.convertJsonToList(responseJson, RatingDto.class);

        assertThat(responseJson).isEqualTo("[{\"id\":\"" + ratingDto.getId() + "\",\"score\":8.6,\"review\":\"Test rating\",\"comments\":[{\"id\":\"" + commentDto.getId() + "\",\"text\":\"Great comment!\",\"rating_id\":\"" + ratingId + "\",\"user_id\":\"" + commentDto.getUserId() + "\"}],\"movie_id\":\"" + ratingDto.getMovieId() + "\",\"user_id\":\"" + ratingDto.getUserId() + "\"}]");
        assertThat(response).isNotEmpty();
    }

    @Test
    void createRating_ratingCreated_returnsResponseWithCreatedRating() throws Exception {
        Mockito.when(ratingFacade.createRating(ratingDto)).thenReturn(ratingDto);

        String inputJson = ObjectConverter.convertObjectToJson(ratingDto);
        MvcResult result = mockMvc.perform(post("/ratings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(inputJson))
                .andExpect(status().isCreated())
                .andReturn();

        String responseJson = result.getResponse().getContentAsString(StandardCharsets.UTF_8);
        RatingDto response = ObjectConverter.convertJsonToObject(responseJson, RatingDto.class);

        assertThat(responseJson).isEqualTo("{\"id\":\"" + ratingDto.getId() + "\",\"score\":8.6,\"review\":\"Test rating\",\"comments\":[{\"id\":\"" + commentDto.getId() + "\",\"text\":\"Great comment!\",\"rating_id\":\"" + ratingId + "\",\"user_id\":\"" + commentDto.getUserId() + "\"}],\"movie_id\":\"" + ratingDto.getMovieId() + "\",\"user_id\":\"" + ratingDto.getUserId() + "\"}");
        assertThat(response).isEqualTo(ratingDto);
    }

    @Test
    void updateRating_ratingUpdated_returnsResponseWithUpdatedRating() throws Exception {
        ratingDto.setScore(3.6);
        Mockito.when(ratingFacade.updateRating(ratingDto.getId(), ratingDto)).thenReturn(ratingDto);

        String inputJson = ObjectConverter.convertObjectToJson(ratingDto);
        MvcResult result = mockMvc.perform(put("/ratings/{id}", ratingDto.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(inputJson))
                .andExpect(status().isOk())
                .andReturn();

        String responseJson = result.getResponse().getContentAsString(StandardCharsets.UTF_8);
        RatingDto response = ObjectConverter.convertJsonToObject(responseJson, RatingDto.class);

        assertThat(responseJson).isEqualTo("{\"id\":\"" + ratingDto.getId() + "\",\"score\":3.6,\"review\":\"Test rating\",\"comments\":[{\"id\":\"" + commentDto.getId() + "\",\"text\":\"Great comment!\",\"rating_id\":\"" + ratingId + "\",\"user_id\":\"" + commentDto.getUserId() + "\"}],\"movie_id\":\"" + ratingDto.getMovieId() + "\",\"user_id\":\"" + ratingDto.getUserId() + "\"}");
        assertThat(response).isEqualTo(ratingDto);
    }

    @Test
    void deleteRating_ratingDeleted_returnsResponseWithNoContent() throws Exception {
        Mockito.doNothing().when(ratingFacade).deleteRating(ratingDto.getId());

        String result = mockMvc.perform(delete("/ratings/{id}", ratingDto.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        Mockito.verify(ratingFacade, Mockito.times(1)).deleteRating(ratingDto.getId());
    }
}
