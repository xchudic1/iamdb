package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Comment;
import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.data.repository.CommentRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CommentServiceTest {

    @Mock
    private CommentRepository commentRepository;

    @InjectMocks
    private CommentService commentService;

    private Comment comment;
    private Rating rating;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        rating = new Rating(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), 8.0, "Very nice movie");
        comment = new Comment(UUID.randomUUID(), rating, UUID.randomUUID(), "Great movie!");
    }

    @Test
    void createComment() {
        when(commentRepository.save(any(Comment.class))).thenReturn(comment);

        Comment savedComment = commentService.createComment(comment);
        assertNotNull(savedComment);
        assertEquals(comment.getText(), savedComment.getText());
    }

    @Test
    void deleteComment() {
        UUID id = comment.getId();
        when(commentRepository.existsById(id)).thenReturn(true);
        doNothing().when(commentRepository).deleteById(id);

        assertDoesNotThrow(() -> commentService.deleteComment(id));
        verify(commentRepository, times(1)).deleteById(id);
    }

    @Test
    void deleteCommentNotFound() {
        UUID id = UUID.randomUUID();
        when(commentRepository.existsById(id)).thenReturn(false);

        assertThrows(ResourceNotFoundException.class, () -> commentService.deleteComment(id));
    }
}
