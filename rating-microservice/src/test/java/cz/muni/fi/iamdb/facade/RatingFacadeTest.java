package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.CommentDto;
import cz.muni.fi.iamdb.api.RatingDto;
import cz.muni.fi.iamdb.data.model.Comment;
import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.mappers.RatingMapper;
import cz.muni.fi.iamdb.service.RatingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
final class RatingFacadeTest {
    @Mock
    private RatingService ratingService;

    @Mock
    private RatingMapper ratingMapper;

    @InjectMocks
    private RatingFacade ratingFacade;

    private RatingDto ratingDto;
    private Rating rating;
    private UUID ratingId;
    private CommentDto commentDto;
    private Comment comment;

    @BeforeEach
    void setUp() {
        ratingId = UUID.randomUUID();
        commentDto = new CommentDto(UUID.randomUUID(), ratingId, UUID.randomUUID(), "Great comment!");
        ratingDto = new RatingDto(ratingId, UUID.randomUUID(), UUID.randomUUID(), 1.2, "Horrible movie", List.of(commentDto));
        comment = new Comment(UUID.randomUUID(), rating, UUID.randomUUID(), "Great comment!");
        rating = new Rating(ratingId, ratingDto.getMovieId(), ratingDto.getUserId(), ratingDto.getScore(), ratingDto.getReview());
        Set<Comment> comments = new HashSet<>();
        comments.add(comment);
        rating.setComments(comments);
    }

    @Test
    void findById_ratingFound_returnsRating() {
        Mockito.when(ratingService.findById(ratingId)).thenReturn(rating);
        Mockito.when(ratingMapper.mapToDto(rating)).thenReturn(ratingDto);

        RatingDto foundRating = ratingFacade.findById(ratingId);

        assertThat(foundRating).isEqualTo(ratingDto);
    }

    @Test
    void findAll_returnsAllRatings() {
        List<Rating> ratingList = List.of(rating);
        List<RatingDto> ratingDtos = List.of(ratingDto);

        Mockito.when(ratingService.findAll()).thenReturn(ratingList);
        Mockito.when(ratingMapper.mapToList(ratingList)).thenReturn(ratingDtos);

        List<RatingDto> foundRatings = ratingFacade.findAll();

        assertThat(foundRatings).isEqualTo(ratingDtos);
    }

    @Test
    void createRating_ratingCreated_returnsRatingDto() {
        Mockito.when(ratingMapper.mapToEntity(ratingDto)).thenReturn(rating);
        Mockito.when(ratingService.saveRating(rating)).thenReturn(rating);
        Mockito.when(ratingMapper.mapToDto(rating)).thenReturn(ratingDto);

        RatingDto createdRating = ratingFacade.createRating(ratingDto);

        assertThat(createdRating).isEqualTo(ratingDto);
    }

    @Test
    void updateRating_ratingUpdated_returnsRatingDto() {
        Mockito.when(ratingMapper.mapToEntity(ratingDto)).thenReturn(rating);
        Mockito.when(ratingService.updateRating(ratingId, rating)).thenReturn(rating);
        Mockito.when(ratingMapper.mapToDto(rating)).thenReturn(ratingDto);

        RatingDto updatedRating = ratingFacade.updateRating(ratingId, ratingDto);

        assertThat(updatedRating).isEqualTo(ratingDto);
        Mockito.verify(ratingService).updateRating(ratingId, rating);
    }

    @Test
    void updateRating_ratingUpdateFails_throwsException() {
        Mockito.when(ratingMapper.mapToEntity(ratingDto)).thenReturn(rating);
        Mockito.when(ratingService.updateRating(ratingId, rating)).thenReturn(null);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> ratingFacade.updateRating(ratingId, ratingDto));

        assertEquals("Failed to update rating", exception.getMessage());
    }

    @Test
    void deleteRating_ratingDeleted() {
        Mockito.doNothing().when(ratingService).deleteRating(ratingId);

        ratingFacade.deleteRating(ratingId);

        Mockito.verify(ratingService, times(1)).deleteRating(ratingId);
    }

    @Test
    void findAllByMovie_returnsAllRatingsByMovie() {
        List<Rating> ratingList = List.of(rating);
        List<RatingDto> ratingDtos = List.of(ratingDto);

        Mockito.when(ratingService.findAllByMovie(ratingDto.getMovieId())).thenReturn(ratingList);
        Mockito.when(ratingMapper.mapToList(ratingList)).thenReturn(ratingDtos);

        List<RatingDto> foundRatings = ratingFacade.findAllByMovie(ratingDto.getMovieId());

        assertThat(foundRatings).isEqualTo(ratingDtos);
    }

    @Test
    void findAllByUser_returnsAllRatingsByUser() {
        List<Rating> ratingList = List.of(rating);
        List<RatingDto> ratingDtos = List.of(ratingDto);

        Mockito.when(ratingService.findAllByUser(ratingDto.getUserId())).thenReturn(ratingList);
        Mockito.when(ratingMapper.mapToList(ratingList)).thenReturn(ratingDtos);

        List<RatingDto> foundRatings = ratingFacade.findAllByUser(ratingDto.getUserId());

        assertThat(foundRatings).isEqualTo(ratingDtos);
    }
}
