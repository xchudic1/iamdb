package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.data.repository.RatingRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
final class RatingServiceTest {
    @Mock
    private RatingRepository ratingRepository;

    @InjectMocks
    private RatingService ratingService;

    @Test
    void createRating_ratingCreated_returnsRating(){
        UUID id = UUID.randomUUID();
        Rating newRating = new Rating(id, UUID.randomUUID(), UUID.randomUUID(), 8.6, "Test rating");
        Mockito.when(ratingRepository.save(newRating)).thenReturn(newRating);

        Rating createdRating = ratingService.saveRating(newRating);

        assertThat(createdRating).isEqualTo(newRating);
    }

    @Test
    void updateRating_ratingUpdated_returnsRating(){
        UUID id = UUID.randomUUID();
        Rating toUpdateRating = new Rating(id, UUID.randomUUID(), UUID.randomUUID(), 8.6, "Updated review");
        Mockito.when(ratingRepository.existsById(id)).thenReturn(true);
        Mockito.when(ratingRepository.save(toUpdateRating)).thenReturn(toUpdateRating);

        Rating updatedRating = ratingService.updateRating(id, toUpdateRating);

        assertThat(updatedRating).isEqualTo(toUpdateRating);
    }

    @Test
    void deleteRating_ratingDeleted_deletesRating(){
        UUID id = UUID.randomUUID();
        Mockito.when(ratingRepository.existsById(id)).thenReturn(true);

        ratingService.deleteRating(id);

        Mockito.verify(ratingRepository).deleteById(id);
    }

    @Test
    void deleteRating_ratingNotFound_throwsException(){
        UUID id = UUID.randomUUID();
        Mockito.when(ratingRepository.existsById(id)).thenReturn(false);

        assertThrows(ResourceNotFoundException.class, () -> ratingService.deleteRating(id));
    }

    @Test
    void findById_ratingFound_returnsRating(){
        UUID id = UUID.randomUUID();
        Rating newRating = new Rating(id, UUID.randomUUID(), UUID.randomUUID(), 8.6, "Test rating");
        Mockito.when(ratingRepository.findById(id)).thenReturn(Optional.of(newRating));

        Rating foundRating = ratingService.findById(id);

        assertThat(foundRating).isEqualTo(newRating);
    }

    @Test
    void findById_ratingNotFound_throwsException(){
        UUID id = UUID.randomUUID();
        Mockito.when(ratingRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> ratingService.findById(id));
    }

    @Test
    void findAll_returnsAllRatings(){
        List<Rating> mockRatings = List.of(new Rating(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), 8.6, "Test rating"));
        Mockito.when(ratingRepository.findAll()).thenReturn(mockRatings);

        List<Rating> ratings = ratingService.findAll();

        assertThat(ratings).isNotEmpty();
        assertThat(ratings).hasSize(1);
    }

    @Test
    void findAllByMovie_returnsAllRatingsByMovie(){
        UUID movieId = UUID.randomUUID();
        List<Rating> mockRatings = List.of(new Rating(UUID.randomUUID(), movieId, UUID.randomUUID(), 8.6, "Test rating"));
        Mockito.when(ratingRepository.findDistinctByMovieId(movieId)).thenReturn(mockRatings);

        List<Rating> ratings = ratingService.findAllByMovie(movieId);

        assertThat(ratings).isNotEmpty();
        assertThat(ratings).hasSize(1);
    }

    @Test
    void findAllByUser_returnsAllRatingsByUser(){
        UUID userId = UUID.randomUUID();
        List<Rating> mockRatings = List.of(new Rating(UUID.randomUUID(), UUID.randomUUID(), userId, 8.6, "Test rating"));
        Mockito.when(ratingRepository.findDistinctByUserId(userId)).thenReturn(mockRatings);

        List<Rating> ratings = ratingService.findAllByUser(userId);

        assertThat(ratings).isNotEmpty();
        assertThat(ratings).hasSize(1);
    }

    @Test
    void findAllByMovie_returnsEmptyListWhenNoRatingsForMovie(){
        UUID movieId = UUID.randomUUID();
        Mockito.when(ratingRepository.findDistinctByMovieId(movieId)).thenReturn(List.of());

        List<Rating> ratings = ratingService.findAllByMovie(movieId);

        assertThat(ratings).isEmpty();
    }

    @Test
    void findAllByUser_returnsEmptyListWhenNoRatingsByUser(){
        UUID userId = UUID.randomUUID();
        Mockito.when(ratingRepository.findDistinctByUserId(userId)).thenReturn(List.of());

        List<Rating> ratings = ratingService.findAllByUser(userId);

        assertThat(ratings).isEmpty();
    }
}
