package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.CommentDto;
import cz.muni.fi.iamdb.api.RatingDto;
import cz.muni.fi.iamdb.facade.RatingFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
final class RatingRestControllerTest {

    @Mock
    private RatingFacade ratingFacade;

    @InjectMocks
    private RatingController ratingController;

    private RatingDto ratingDto;
    private CommentDto commentDto;
    private UUID ratingId;

    @BeforeEach
    void setUp() {
        ratingId = UUID.randomUUID();
        commentDto = new CommentDto(UUID.randomUUID(), ratingId, UUID.randomUUID(), "Great comment!");
        ratingDto = new RatingDto(ratingId, UUID.randomUUID(), UUID.randomUUID(), 8.6, "Test review", List.of(commentDto));
    }

    @Test
    void findById_validRequestBody_returnsResponseWithRating() {
        Mockito.when(ratingFacade.findById(ratingId)).thenReturn(ratingDto);

        ResponseEntity<RatingDto> foundRating = ratingController.findById(ratingId);

        assertThat(foundRating.getBody()).isNotNull();
        assertThat(foundRating.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(foundRating.getBody().getScore()).isEqualTo(8.6);
        assertThat(foundRating.getBody().getComments()).isNotEmpty();
    }

    @Test
    void findAll_validRequestBody_returnsResponseWithRatings() {
        Mockito.when(ratingFacade.findAll()).thenReturn(List.of(ratingDto));

        ResponseEntity<List<RatingDto>> foundRatings = ratingController.findAll();

        assertThat(foundRatings.getBody()).isNotEmpty();
        assertThat(foundRatings.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
    }

    @Test
    void createRating_validRequestBody_returnsResponseWithCreatedRating() {
        Mockito.when(ratingFacade.createRating(ratingDto)).thenReturn(ratingDto);

        ResponseEntity<RatingDto> createdRating = ratingController.createRating(ratingDto);

        assertThat(createdRating.getBody()).isNotNull();
        assertThat(createdRating.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(201));
        assertThat(createdRating.getBody().getScore()).isEqualTo(8.6);
        assertThat(createdRating.getBody().getComments()).isNotEmpty();
    }

    @Test
    void updateRating_validRequestBody_returnsResponseWithUpdatedRating() {
        Mockito.when(ratingFacade.updateRating(ratingId, ratingDto)).thenReturn(ratingDto);

        ratingDto.setScore(3.6);
        ResponseEntity<RatingDto> updatedRating = ratingController.updateRating(ratingId, ratingDto);

        assertThat(updatedRating.getBody()).isNotNull();
        assertThat(updatedRating.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(updatedRating.getBody().getScore()).isEqualTo(3.6);
        assertThat(updatedRating.getBody().getComments()).isNotEmpty();
    }

    @Test
    void deleteRating_validRequestBody_callsDeleteRatingOnFacade() {
        Mockito.doNothing().when(ratingFacade).deleteRating(ratingId);

        ratingController.deleteRating(ratingId);

        Mockito.verify(ratingFacade, Mockito.times(1)).deleteRating(ratingId);
    }

    @Test
    void deleteRating_validRequestBody_returnsResponseWithNoContent() {
        Mockito.doNothing().when(ratingFacade).deleteRating(ratingId);

        ResponseEntity<Void> response = ratingController.deleteRating(ratingId);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(204));
    }

    @Test
    void findAllByMovie_validRequestBody_returnsResponseWithRatings() {
        Mockito.when(ratingFacade.findAllByMovie(ratingDto.getMovieId())).thenReturn(List.of(ratingDto));

        ResponseEntity<List<RatingDto>> foundRatings = ratingController.findAllByMovie(ratingDto.getMovieId());

        assertThat(foundRatings.getBody()).isNotEmpty();
        assertThat(foundRatings.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
    }

    @Test
    void findAllByUser_validRequestBody_returnsResponseWithRatings() {
        Mockito.when(ratingFacade.findAllByUser(ratingDto.getUserId())).thenReturn(List.of(ratingDto));

        ResponseEntity<List<RatingDto>> foundRatings = ratingController.findAllByUser(ratingDto.getUserId());

        assertThat(foundRatings.getBody()).isNotEmpty();
        assertThat(foundRatings.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
    }
}
