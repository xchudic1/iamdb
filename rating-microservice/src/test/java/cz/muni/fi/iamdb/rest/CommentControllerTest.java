package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.CommentDto;
import cz.muni.fi.iamdb.facade.CommentFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CommentControllerTest {

    @Mock
    private CommentFacade commentFacade;

    @InjectMocks
    private CommentController commentController;

    private MockMvc mockMvc;
    private CommentDto commentDto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(commentController).build();
        commentDto = new CommentDto(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "Great movie!");
    }

    @Test
    void createComment() throws Exception {
        when(commentFacade.createComment(any(CommentDto.class))).thenReturn(commentDto);

        mockMvc.perform(post("/comments")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"rating_id\":\"" + commentDto.getRatingId() + "\",\"user_id\":\"" + commentDto.getUserId() + "\",\"text\":\"Great movie!\"}"))
                .andExpect(status().isCreated());
    }

    @Test
    void deleteComment() throws Exception {
        UUID id = UUID.randomUUID();
        doNothing().when(commentFacade).deleteComment(id);

        mockMvc.perform(delete("/comments/{id}", id.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}
