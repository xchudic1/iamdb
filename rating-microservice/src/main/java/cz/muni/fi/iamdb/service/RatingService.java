package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.data.repository.RatingRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RatingService {
    private final RatingRepository ratingRepository;

    @Autowired
    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Transactional(readOnly = true)
    public Rating findById(UUID id) {
        return ratingRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Rating with id: " + id + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<Rating> findAll() {
        return ratingRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Rating> findAllByMovie(UUID movieId) {
        return ratingRepository.findDistinctByMovieId(movieId);
    }

    @Transactional(readOnly = true)
    public List<Rating> findAllByUser(UUID userId) {
        return ratingRepository.findDistinctByUserId(userId);
    }

    @Transactional
    public Rating saveRating(Rating rating) {
        return ratingRepository.save(rating); // handles both create and update
    }

    @Transactional
    public Rating updateRating(UUID id, Rating rating) {
        if (!ratingRepository.existsById(id)) {
            throw new ResourceNotFoundException("Rating with ID " + id + " does not exist to update.");
        }
        rating.setId(id); // Ensure the ID is correct for the update
        return ratingRepository.save(rating);
    }

    @Transactional
    public void deleteRating(UUID id) {
        if (!ratingRepository.existsById(id)) {
            throw new ResourceNotFoundException("Rating with ID " + id + " does not exist.");
        }
        ratingRepository.deleteById(id);
    }
}
