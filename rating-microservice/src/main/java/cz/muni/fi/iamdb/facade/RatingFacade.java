package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.RatingDto;
import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.mappers.RatingMapper;
import cz.muni.fi.iamdb.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@Service
@Transactional
public class RatingFacade {
    private final RatingService ratingService;
    private final RatingMapper ratingMapper;
    private static final Logger logger = Logger.getLogger(RatingFacade.class.getName());

    @Autowired
    public RatingFacade(RatingService ratingService, RatingMapper ratingMapper) {
        this.ratingService = ratingService;
        this.ratingMapper = ratingMapper;
    }

    public RatingDto findById(UUID id) {
        logger.info("Finding rating by ID: " + id);
        Rating rating = ratingService.findById(id);
        RatingDto ratingDto = ratingMapper.mapToDto(rating);
        logger.info("Found rating: " + ratingDto);
        return ratingDto;
    }

    public List<RatingDto> findAll() {
        logger.info("Finding all ratings");
        List<Rating> ratings = ratingService.findAll();
        List<RatingDto> ratingDtos = ratingMapper.mapToList(ratings);
        logger.info("Found ratings: " + ratingDtos);
        return ratingDtos;
    }

    public List<RatingDto> findAllByMovie(UUID movieId) {
        logger.info("Finding ratings by movie ID: " + movieId);
        List<Rating> ratings = ratingService.findAllByMovie(movieId);
        List<RatingDto> ratingDtos = ratingMapper.mapToList(ratings);
        logger.info("Found ratings by movie: " + ratingDtos);
        return ratingDtos;
    }

    public List<RatingDto> findAllByUser(UUID userId) {
        logger.info("Finding ratings by user ID: " + userId);
        List<Rating> ratings = ratingService.findAllByUser(userId);
        List<RatingDto> ratingDtos = ratingMapper.mapToList(ratings);
        logger.info("Found ratings by user: " + ratingDtos);
        return ratingDtos;
    }

    public RatingDto createRating(RatingDto ratingDto) {
        logger.info("Creating rating: " + ratingDto);
        Rating rating = ratingMapper.mapToEntity(ratingDto);
        Rating savedRating = ratingService.saveRating(rating);
        if (savedRating == null) {
            throw new IllegalArgumentException("Failed to save rating");
        }
        RatingDto savedRatingDto = ratingMapper.mapToDto(savedRating);
        logger.info("Created rating: " + savedRatingDto);
        return savedRatingDto;
    }

    public RatingDto updateRating(UUID id, RatingDto ratingDto) {
        logger.info("Updating rating with ID: " + id + " and data: " + ratingDto);
        Rating rating = ratingMapper.mapToEntity(ratingDto);
        rating.setId(id); // Ensure the ID is correct for the update.
        Rating updatedRating = ratingService.updateRating(id, rating);
        if (updatedRating == null) {
            throw new IllegalArgumentException("Failed to update rating");
        }
        RatingDto updatedRatingDto = ratingMapper.mapToDto(updatedRating);
        logger.info("Updated rating: " + updatedRatingDto);
        return updatedRatingDto;
    }

    public void deleteRating(UUID id) {
        logger.info("Deleting rating with ID: " + id);
        ratingService.deleteRating(id);
    }
}
