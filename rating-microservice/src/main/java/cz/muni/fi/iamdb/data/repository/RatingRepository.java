package cz.muni.fi.iamdb.data.repository;

import cz.muni.fi.iamdb.data.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RatingRepository extends JpaRepository<Rating, UUID> {
    List<Rating> findDistinctByUserId(UUID userId);
    List<Rating> findDistinctByMovieId(UUID movieId);
}
