package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.CommentDto;
import cz.muni.fi.iamdb.data.model.Comment;
import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.mappers.CommentMapper;
import cz.muni.fi.iamdb.service.CommentService;
import cz.muni.fi.iamdb.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@Transactional
public class CommentFacade {
    private final CommentService commentService;
    private final RatingService ratingService;
    private final CommentMapper commentMapper;

    @Autowired
    public CommentFacade(CommentService commentService, RatingService ratingService, CommentMapper commentMapper) {
        this.commentService = commentService;
        this.ratingService = ratingService;
        this.commentMapper = commentMapper;
    }

    public CommentDto createComment(CommentDto commentDto) {
        Rating rating = ratingService.findById(commentDto.getRatingId());
        Comment comment = commentMapper.mapToEntity(commentDto);
        comment.setRating(rating);
        Comment savedComment = commentService.createComment(comment);
        return commentMapper.mapToDto(savedComment);
    }

    public void deleteComment(UUID id) {
        commentService.deleteComment(id);
    }

    public void deleteCommentsByRating(UUID ratingId) {
        commentService.deleteCommentsByRating(ratingId);
    }
}
