package cz.muni.fi.iamdb.data.model;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "comments")
public class Comment implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_comment", updatable = false, nullable = false)
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rating_id", nullable = false)
    private Rating rating;

    @Column(name = "user_id", nullable = false)
    private UUID userId;

    @Column(name = "text", nullable = false, length = 1000)
    private String text;

    public Comment() {
    }

    public Comment(UUID id, Rating rating, UUID userId, String text) {
        this.id = id;
        this.rating = rating;
        this.userId = userId;
        this.text = text;
    }


    public UUID getId() {
        return id;
    }

    public Rating getRating() {
        return rating;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public void setText(String text) {
        this.text = text;
    }
}
