package cz.muni.fi.iamdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RatingMicroserviceApp {
    public static void main(String[] args) {
        SpringApplication.run(RatingMicroserviceApp.class, args);
    }
}
