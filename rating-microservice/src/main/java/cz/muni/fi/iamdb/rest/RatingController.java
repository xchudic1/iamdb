package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.RatingDto;
import cz.muni.fi.iamdb.facade.RatingFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/ratings")
public class RatingController {
    private final RatingFacade ratingFacade;

    @Autowired
    public RatingController(RatingFacade ratingFacade) {
        this.ratingFacade = ratingFacade;
    }

    @Operation(summary = "Find rating by ID", description = "Returns a single rating with comments")
    @ApiResponse(responseCode = "200", description = "Rating found")
    @ApiResponse(responseCode = "404", description = "Rating not found")
    @GetMapping("/{id}")
    public ResponseEntity<RatingDto> findById(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(ratingFacade.findById(id));
    }

    @Operation(summary = "List all ratings", description = "Returns a list of ratings with comments")
    @ApiResponse(responseCode = "200", description = "Successful operation")
    @GetMapping
    public ResponseEntity<List<RatingDto>> findAll() {
        return ResponseEntity.ok(ratingFacade.findAll());
    }

    @Operation(summary = "List all ratings by movie", description = "Returns a list of ratings for a movie with comments")
    @ApiResponse(responseCode = "200", description = "Successful operation")
    @GetMapping("/movie/{movieId}")
    public ResponseEntity<List<RatingDto>> findAllByMovie(@PathVariable("movieId") UUID movieId) {
        return ResponseEntity.ok(ratingFacade.findAllByMovie(movieId));
    }

    @Operation(summary = "List all ratings by user", description = "Returns a list of ratings by a user with comments")
    @ApiResponse(responseCode = "200", description = "Successful operation")
    @GetMapping("/user/{userId}")
    public ResponseEntity<List<RatingDto>> findAllByUser(@PathVariable("userId") UUID userId) {
        return ResponseEntity.ok(ratingFacade.findAllByUser(userId));
    }

    @Operation(summary = "Create a new rating", description = "Creates a new rating for a movie with score validation.")
    @ApiResponse(responseCode = "201", description = "Rating created")
    @ApiResponse(responseCode = "400", description = "Bad Request - Score must be a double between 1 and 10.")
    @PostMapping
    public ResponseEntity<RatingDto> createRating(@RequestBody RatingDto ratingDto) {
        RatingDto createdRating = ratingFacade.createRating(ratingDto);
        return new ResponseEntity<>(createdRating, HttpStatus.CREATED);
    }

    @Operation(summary = "Update an existing rating", description = "Updates a rating identified by ID with score validation.")
    @ApiResponse(responseCode = "200", description = "Rating updated")
    @ApiResponse(responseCode = "400", description = "Bad Request - Score must be a double between 1 and 10.")
    @ApiResponse(responseCode = "404", description = "Rating not found")
    @PutMapping("/{id}")
    public ResponseEntity<RatingDto> updateRating(@PathVariable UUID id, @RequestBody RatingDto ratingDto) {
        ratingDto.setId(id);
        RatingDto updatedRating = ratingFacade.updateRating(id, ratingDto);
        return ResponseEntity.ok(updatedRating);
    }

    @Operation(summary = "Delete a rating", description = "Deletes a rating identified by ID")
    @ApiResponse(responseCode = "204", description = "Rating deleted")
    @ApiResponse(responseCode = "404", description = "Rating not found")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRating(@PathVariable UUID id) {
        ratingFacade.deleteRating(id);
        return ResponseEntity.noContent().build();
    }
}
