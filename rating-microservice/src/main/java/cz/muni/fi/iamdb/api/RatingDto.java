package cz.muni.fi.iamdb.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class RatingDto {

    private UUID id;
    @JsonProperty("movie_id")
    private UUID movieId;
    @JsonProperty("user_id")
    private UUID userId;
    private Double score;
    private String review;
    private List<CommentDto> comments;

    public RatingDto(UUID id, UUID movieId, UUID userId, Double score, String review, List<CommentDto> comments) {
        this.id = id;
        this.movieId = movieId;
        this.userId = userId;
        this.score = score;
        this.review = review;
        this.comments = comments;
    }

    public RatingDto() {}

    // Getters and setters

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getMovieId() {
        return movieId;
    }

    public void setMovieId(UUID movieId) {
        this.movieId = movieId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        if (score < 1.0 || score > 10.0) {
            throw new IllegalArgumentException("Score must be a number between 1 and 10.");
        }
        this.score = score;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public List<CommentDto> getComments() {
        return comments;
    }

    public void setComments(List<CommentDto> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RatingDto ratingDto = (RatingDto) o;
        return Objects.equals(id, ratingDto.id) &&
                Objects.equals(movieId, ratingDto.movieId) &&
                Objects.equals(userId, ratingDto.userId) &&
                Objects.equals(score, ratingDto.score) &&
                Objects.equals(review, ratingDto.review) &&
                Objects.equals(comments, ratingDto.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, movieId, userId, score, review, comments);
    }
}
