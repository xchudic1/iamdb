package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.CommentDto;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import cz.muni.fi.iamdb.facade.CommentFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/comments")
public class CommentController {
    private final CommentFacade commentFacade;

    @Autowired
    public CommentController(CommentFacade commentFacade) {
        this.commentFacade = commentFacade;
    }

    @Operation(summary = "Create a new comment", description = "Creates a new comment for a rating.")
    @ApiResponse(responseCode = "201", description = "Comment created")
    @PostMapping
    public ResponseEntity<CommentDto> createComment(@RequestBody CommentDto commentDto) {
        try {
            CommentDto createdComment = commentFacade.createComment(commentDto);
            return new ResponseEntity<>(createdComment, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Delete a comment", description = "Deletes a comment identified by ID")
    @ApiResponse(responseCode = "204", description = "Comment deleted")
    @ApiResponse(responseCode = "404", description = "Comment not found")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteComment(@PathVariable UUID id) {
        try {
            commentFacade.deleteComment(id);
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
