package cz.muni.fi.iamdb.data.model;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "ratings", indexes = {
        @Index(name = "idx_movie_id", columnList = "movie_id"),
        @Index(name = "idx_user_id", columnList = "user_id")
})
public class Rating implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_rating", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "movie_id", nullable = false)
    private UUID movieId;

    @Column(name = "user_id", nullable = false)
    private UUID userId;

    @Column(name = "score")
    private Double score;

    @Column(name = "review", length = 1000)
    private String review;

    @OneToMany(mappedBy = "rating", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<Comment> comments = new HashSet<>();

    public Rating() {
    }

    public Rating(UUID id, UUID movieId, UUID userId, Double score, String review) {
        this.id = id;
        this.movieId = movieId;
        this.userId = userId;
        this.score = score;
        this.review = review;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getMovieId() {
        return movieId;
    }

    public void setMovieId(UUID movieId) {
        this.movieId = movieId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        comments.add(comment);
        comment.setRating(this);
    }

    public void removeComment(Comment comment) {
        comments.remove(comment);
        comment.setRating(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rating rating)) return false;
        return Objects.equals(id, rating.getId()) &&
                Objects.equals(movieId, rating.getMovieId()) &&
                Objects.equals(userId, rating.getUserId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, movieId, userId);
    }

    @Override
    public String toString() {
        return "Rating{" +
                "id=" + id +
                ", movieId=" + movieId +
                ", userId=" + userId +
                ", score=" + score +
                ", review='" + review + '\'' +
                ", comments=" + comments +
                '}';
    }
}
