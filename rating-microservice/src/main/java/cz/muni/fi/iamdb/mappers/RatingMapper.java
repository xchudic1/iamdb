package cz.muni.fi.iamdb.mappers;

import cz.muni.fi.iamdb.api.CommentDto;
import cz.muni.fi.iamdb.api.RatingDto;
import cz.muni.fi.iamdb.data.model.Comment;
import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.service.RatingService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring", uses = {CommentMapper.class, RatingMapper.RatingMapperHelper.class})
public interface RatingMapper {

    @Mapping(source = "comments", target = "comments")
    RatingDto mapToDto(Rating rating);

    @Mapping(source = "comments", target = "comments")
    Rating mapToEntity(RatingDto ratingDto);

    List<RatingDto> mapToList(List<Rating> ratings);

    @Component
    class RatingMapperHelper {
        @Autowired
        private RatingService ratingService;

        public Rating map(UUID ratingId) {
            return ratingService.findById(ratingId);
        }

        public UUID map(Rating rating) {
            return rating.getId();
        }
    }
}
