package cz.muni.fi.iamdb.mappers;

import cz.muni.fi.iamdb.api.CommentDto;
import cz.muni.fi.iamdb.data.model.Comment;
import cz.muni.fi.iamdb.data.model.Rating;
import cz.muni.fi.iamdb.service.RatingService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = CommentMapper.CommentMapperHelper.class)
public interface CommentMapper {
    @Mapping(source = "rating.id", target = "ratingId")
    CommentDto mapToDto(Comment comment);

    @Mapping(source = "ratingId", target = "rating")
    Comment mapToEntity(CommentDto commentDto);

    @Component
    class CommentMapperHelper {
        @Autowired
        private RatingService ratingService;

        public Rating map(UUID ratingId) {
            return ratingService.findById(ratingId);
        }

        public UUID map(Rating rating) {
            return rating.getId();
        }
    }
}
