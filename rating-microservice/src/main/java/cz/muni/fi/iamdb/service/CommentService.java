package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Comment;
import cz.muni.fi.iamdb.data.repository.CommentRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class CommentService {
    private final CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Transactional
    public Comment createComment(Comment comment) {
        return commentRepository.save(comment);
    }

    @Transactional
    public void deleteComment(UUID id) {
        if (!commentRepository.existsById(id)) {
            throw new ResourceNotFoundException("Comment with ID " + id + " does not exist.");
        }
        commentRepository.deleteById(id);
    }

    @Transactional
    public void deleteCommentsByRating(UUID ratingId) {
        commentRepository.deleteByRatingId(ratingId);
    }
}
