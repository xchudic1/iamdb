package cz.muni.fi.iamdb.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class CommentDto {

    private UUID id;
    @JsonProperty("rating_id")
    private UUID ratingId;
    @JsonProperty("user_id")
    private UUID userId;
    private String text;

    public CommentDto(UUID id, UUID ratingId, UUID userId, String text) {
        this.id = id;
        this.ratingId = ratingId;
        this.userId = userId;
        this.text = text;
    }

    public CommentDto() {

    }

    public UUID getId() {
        return id;
    }

    public UUID getRatingId() {
        return ratingId;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setRatingId(UUID ratingId) {
        this.ratingId = ratingId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentDto that = (CommentDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(ratingId, that.ratingId) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ratingId, userId, text);
    }
}
