# Rating Microservice

## Overview

The Rating Microservice is designed to manage user ratings and reviews for movies. It enables users to express their opinions on movies through ratings and textual reviews. This service is a part of four microservices for Movie Recommender.

## Technologies Used

- Spring Boot: for the microservice framework.
- MapStruct: for object mapping.

## Getting Started

To get the microservice running on your local machine for development and testing purposes, follow these steps:

```bash
mvn clean install
mvn spring-boot:run
```

To run microservice using Docker and connect to PostgreSQL database:

- Make sure your docker engine is running.

- Navigate to parent project directory.

```bash
cd ..
docker-compose up
```

## API Endpoints

You can use swagger with url <http://localhost:8002/api/v1/swagger-ui/index.html#/> to test the endpoints.

- (your host, port may be different depending on your env)

The rating microservice endpoints work in [localhost:8002/api/v1/ratings](http://localhost:8002/api/v1/ratings).

This section details the available API endpoints for the Rating Microservice, including the expected request bodies and the responses for each operation.

### Rate a Movie
**Endpoint**: POST /api/v1/ratings

**Description**: Allows a user to create a new rating for a movie. The _score_ value must be between 1 and 10.

**Request Body Example**:

```bash
{
  "movieId": 1717e78e-61ae-4cd0-9f1d-da82f35911d4,
  "userId": 1717e78e-61ae-4cd0-9f1d-da82f35911d4,
  "score": 8.5,
  "review": "It definitely ruined me."
}
```
**Response**: Returns the newly created Rating object along with a _200 OK_ status. _Id_ property is automatically set.
```bash
{
    "id": 1917e78e-61ae-4cd0-9f1d-da82f35911d6,
    "score": 8.5,
    "review": "It definitely ruined me.",
    "movie_id": 1717e78e-61ae-4cd0-9f1d-da82f35911d4,
    "user_id": 1717e78e-61ae-4cd0-9f1d-da82f35911d4
}
```

### Update a Rating
**Endpoint**: PUT /api/v1/ratings/{id}

**Description**: Updates an existing rating. The {id} in the URL is the ID of the rating to update.

**Request Body Example**:
```bash
{
  "movieId": 1717e78e-61ae-4cd0-9f1d-da82f35911d4,
  "userId": 1717e78e-61ae-4cd0-9f1d-da82f35911d4,
  "score": 9.0,
  "review": "After a second watch, I've updated my score."
}
```
**Response**: Returns the updated Rating object with 200 OK code.

### Delete a Rating
**Endpoint**: DELETE /api/v1/ratings/{id}

**Description**: Deletes a specific rating by its ID.

**Response**: A successful operation will return a _204 No Content_ status.

### Find a Specific Rating
**Endpoint**: GET /api/v1/ratings/{id}

**Description**: Fetches details of a specific rating by its ID.

**Response**: Returns the requested Rating object.

### Find All Ratings
**Endpoint**: GET /api/v1/ratings

**Description**: Retrieves a list of all ratings in the system.

**Response**: Returns a list of Rating objects.

### Error Handling
Responses to unsuccessful requests will include an appropriate HTTP status code and JSON object with details.

```bash
{
"timestamp": "2024-03-31T13:04:00.1124871",
"status": "INTERNAL_SERVER_ERROR",
"message": "Request method 'POST' is not supported",
"path": "/api/v1/ratings-microservice/delete/2"
}
```

### Clearing and Seeding the Database

In order to clear or seed the dockerized ratings database, you can run the following docker commands.

To clear run the following command (from the repo root, or change the sqeal file's path) (substitute your credentials accordingly):

```bash
# Copy the SQL script to the Docker container
docker cp ./sql/clear_rating_database.sql rating_postgres:/clear_rating_database.sql

# Execute the SQL script inside the Docker container
docker exec -it rating_postgres psql -U rating_user -d rating_database -a -f /clear_rating_database.sql

```

To seed:
```bash
# Copy the SQL script to the Docker container
docker cp ./sql/seed_rating_database.sql rating_postgres:/seed_rating_database.sql

# Execute the SQL script inside the Docker container
docker exec -it rating_postgres psql -U rating_user -d rating_database -a -f /seed_rating_database.sql

```
