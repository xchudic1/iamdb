from locust import HttpUser


class MovieRecommenderService(HttpUser):
    host = "http://localhost:8000/api/v1"
    abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def recommend_movie(self, movie_id):
        response = self.client.get(f"{self.host}/recommend/{movie_id}")
        if response.status_code == 200:
            return response
        print(response.text)
