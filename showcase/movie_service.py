from locust import HttpUser


class MovieService(HttpUser):
    host = "http://localhost:8001/api/v1"
    abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_all_movies(self):
        response = self.client.get(f"{self.host}/movies")
        return response

    def get_movie_details(self, movie_id):
        response = self.client.get(f"{self.host}/movies/{movie_id}")
        return response

    def create_movie(self, movie):
        response = self.client.post(f"{self.host}/movies", json=movie)
        return response

    def update_movie(self, movie_id, movie):
        response = self.client.put(f"{self.host}/{movie_id}", json=movie)
        return response

    def delete_movie(self, movie_id):
        response = self.client.delete(f"{self.host}/{movie_id}")
        return response

    def seed_database(self):
        response = self.client.post(f"{self.host}/movies/production/seed")
        return response

    def clean_database(self):
        response = self.client.post(f"{self.host}/movies/production/clean")
        return response



