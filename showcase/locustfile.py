from locust import HttpUser, task, between
import json
import random
from movie_service import MovieService
from rating_service import RatingService
from movie_recommender_service import MovieRecommenderService


class MovieRecommenderShowcase(HttpUser):
    tasks = []
    host = "http://localhost:8080"
    wait_time = between(1, 3)

    def __init__(self, *args, **kwargs):
        self.movie_service = MovieService(*args, **kwargs)
        self.rating_service = RatingService(*args, **kwargs)
        self.movie_recommender_service = MovieRecommenderService(*args, **kwargs)
        self.movie_service.seed_database()
        super().__init__(*args, **kwargs)

    def __del__(self):
        self.movie_service.clean_database()
        self.movie_service.client.close()
        self.rating_service.client.close()
        self.movie_recommender_service.client.close()

    @task
    def pick_movie_show_details(self):
        # Get all movies
        movies_response = self.movie_service.get_all_movies()
        if movies_response.status_code != 200:
            print(movies_response.text)
            return

        all_movies = json.loads(movies_response.content)
        # Get a random movie
        random_movie = random.choice(all_movies)

        # Show movie details
        movie_response = self.movie_service.get_movie_details(random_movie["id"])
        if movie_response.status_code != 200:
            print(movie_response.text)
            return

    @task
    def rate_random_movie(self):
        # Get all movies
        movies_response = self.movie_service.get_all_movies()
        if movies_response.status_code != 200:
            print(movies_response.text)
            return

        all_movies = json.loads(movies_response.content)
        # Get a random movie
        random_movie = random.choice(all_movies)

        # Rate the movie
        rating = {
            "movieId": random_movie["id"],
            "score": random.randint(1, 10),
            "review": "Great movie!",
            "userId": random_movie["id"],
        }
        self.rating_service.rate_movie(random_movie["id"], rating)

    @task
    def recommend_movie(self):
        # Get all movies
        movies_response = self.movie_service.get_all_movies()
        if movies_response.status_code != 200:
            print(movies_response.text)
            return

        all_movies = json.loads(movies_response.content)
        # Get a random movie
        random_movie = random.choice(all_movies)

        # Recommend a movie
        self.movie_recommender_service.recommend_movie(random_movie["id"])
