# Test scenario
The goal of this scenario is to showcase the usage of Movie Recommender Application.

# Usage

* show_movie_ratings 
* rate_random_movie  
* recommend_movie 


# Setup

First, you need to have Python 3 installed. (It is advised to have a virtual environment set up)

**Step 1:** Clone the repository

run ```pip install -r requirements.txt```

After this setup is done, you can run locust using

locust --headless --users 10 --spawn-rate 1