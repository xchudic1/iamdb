from locust import HttpUser


class RatingService(HttpUser):
    host = "http://localhost:8002/api/v1"
    abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_ratings_for_movie(self, movie_id):
        response = self.client.get(f"{self.host}/ratings/{movie_id}")
        if response.status_code == 200:
            return response
        print(response.text)

    def rate_movie(self, movie_id, rating):
        response = self.client.post(f"{self.host}/ratings", json=rating)
        if response.status_code == 200:
            return response
        print(response.text)
