BEGIN;

TRUNCATE movie CASCADE;
TRUNCATE genre CASCADE;

INSERT INTO genre (genre_id, name) VALUES
    ('d1b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Drama'),
    ('d2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Action'),
    ('d3b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Comedy'),
    ('d4b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Horror'),
    ('d5b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Thriller'),
    ('d6b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Romance'),
    ('d7b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Sci-Fi'),
    ('d8b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Fantasy'),
    ('d9b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Mystery'),
    ('d0b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b', 'Documentary');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('ae5f4109-3575-458f-88c4-e3e4946b06f7', 'The Shawshank Redemption', 1994, 142, 'Two imprisoned');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('1cd25723-f25b-4f1c-9235-8cf84745be77', 'ae5f4109-3575-458f-88c4-e3e4946b06f7', 'https://www.imdb.com/title/tt0111161/mediaviewer/rm10105600'),
    ('1cd25724-f25b-4f1c-9235-8cf84745be77', 'ae5f4109-3575-458f-88c4-e3e4946b06f7', 'https://www.imdb.com/title/tt0111161/mediaviewer/rm1010'),
    ('1cd25725-f25b-4f1c-9235-8cf84745be77', 'ae5f4109-3575-458f-88c4-e3e4946b06f7', 'https://www.imdb.com/title/tt0111161/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('ae5f4109-3575-458f-88c4-e3e4946b06f7', 'd1b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('ae5f4109-3575-458f-88c4-e3e4946b06f7', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('b2ed63fe-02c7-4c3a-9ffb-ef29f93b2dcd', 'The Godfather', 1972, 175, 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('8c56be3f-dd79-4ff4-9807-2e1d1107d96c', 'b2ed63fe-02c7-4c3a-9ffb-ef29f93b2dcd', 'https://www.imdb.com/title/tt0068646/mediaviewer/rm10105600'),
    ('8c56be4f-dd79-4ff4-9807-2e1d1107d96c', 'b2ed63fe-02c7-4c3a-9ffb-ef29f93b2dcd', 'https://www.imdb.com/title/tt0068646/mediaviewer/rm1010'),
    ('8c56be5f-dd79-4ff4-9807-2e1d1107d96c', 'b2ed63fe-02c7-4c3a-9ffb-ef29f93b2dcd', 'https://www.imdb.com/title/tt0068646/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('b2ed63fe-02c7-4c3a-9ffb-ef29f93b2dcd', 'd1b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('b2ed63fe-02c7-4c3a-9ffb-ef29f93b2dcd', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('1326ad37-b42c-41c5-83d5-a54dc2994d42', 'The Dark Knight', 2008, 152, 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('0e00cfef-410d-4090-88e3-f6bb2ccdea21', '1326ad37-b42c-41c5-83d5-a54dc2994d42', 'https://www.imdb.com/title/tt0468569/mediaviewer/rm10105600'),
    ('0e00cfef-410d-4090-88e3-f6bb2ccdea22', '1326ad37-b42c-41c5-83d5-a54dc2994d42', 'https://www.imdb.com/title/tt0468569/mediaviewer/rm1010'),
    ('0e00cfef-410d-4090-88e3-f6bb2ccdea23', '1326ad37-b42c-41c5-83d5-a54dc2994d42', 'https://www.imdb.com/title/tt0468569/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('1326ad37-b42c-41c5-83d5-a54dc2994d42', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('1326ad37-b42c-41c5-83d5-a54dc2994d42', 'd5b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('4414e0ad-a947-44d0-ac5e-4fb132fe77d5', 'The Lord of the Rings: The Return of the King', 2003, 201, 'Gandalf and Aragorn lead the World');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('cff195c1-94c7-4b36-b8ba-a03a07bd4489', '4414e0ad-a947-44d0-ac5e-4fb132fe77d5', 'https://www.imdb.com/title/tt0167260/mediaviewer/rm10105600'),
    ('cff195c1-94c7-4b36-b8ba-a03a08bd4489', '4414e0ad-a947-44d0-ac5e-4fb132fe77d5', 'https://www.imdb.com/title/tt0167260/mediaviewer/rm1010'),
    ('cff195c1-94c7-4b36-b8ba-a03a06bd4489', '4414e0ad-a947-44d0-ac5e-4fb132fe77d5', 'https://www.imdb.com/title/tt0167260/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('4414e0ad-a947-44d0-ac5e-4fb132fe77d5', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('4414e0ad-a947-44d0-ac5e-4fb132fe77d5', 'd8b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('a42eb540-afca-4ea7-98ad-b5dd5f39963e', 'The Lord of the Rings: The Fellowship of the Ring', 2001, 178, 'A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('e82da507-2177-4271-bebf-43b67d6483b7', 'a42eb540-afca-4ea7-98ad-b5dd5f39963e', 'https://www.imdb.com/title/tt0120737/mediaviewer/rm10105600'),
    ('e82da507-2177-4271-bebf-43b67d6483b6', 'a42eb540-afca-4ea7-98ad-b5dd5f39963e', 'https://www.imdb.com/title/tt0120737/mediaviewer/rm1010'),
    ('e82da507-2177-4271-bebf-43b67d6483b8', 'a42eb540-afca-4ea7-98ad-b5dd5f39963e', 'https://www.imdb.com/title/tt0120737/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('a42eb540-afca-4ea7-98ad-b5dd5f39963e', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('a42eb540-afca-4ea7-98ad-b5dd5f39963e', 'd8b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('daae78a8-4103-47c9-9f1b-b38303c7f47f', 'The Lord of the Rings: The Two Towers', 2002, 179, 'While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum, the divided fellowship makes a stand against Sauron');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('9aa39101-ea63-4d52-a460-b5501ee90ecb', 'daae78a8-4103-47c9-9f1b-b38303c7f47f', 'https://www.imdb.com/title/tt0167261/mediaviewer/rm10105600'),
    ('9aa39102-ea63-4d52-a460-b5501ee90ecb', 'daae78a8-4103-47c9-9f1b-b38303c7f47f', 'https://www.imdb.com/title/tt0167261/mediaviewer/rm1010'),
    ('9aa39103-ea63-4d52-a460-b5501ee90ecb', 'daae78a8-4103-47c9-9f1b-b38303c7f47f', 'https://www.imdb.com/title/tt0167261/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('daae78a8-4103-47c9-9f1b-b38303c7f47f', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('daae78a8-4103-47c9-9f1b-b38303c7f47f', 'd8b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('19a8e571-ebec-45be-bdbe-5008a273b9b9', 'The Matrix', 1999, 136, 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('19a8e571-ebec-45be-bdbe-5008a273b9b9', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('19a8e571-ebec-45be-bdbe-5008a273b9b9', 'd7b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('99ba2239-d61f-455c-9bf6-5eb14c42fa1b', 'Inception', 2010, 148, 'A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O.');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('1018b357-c02d-4066-b9af-908b45a03f3a', '99ba2239-d61f-455c-9bf6-5eb14c42fa1b', 'https://www.imdb.com/title/tt1375666/mediaviewer/rm10105600'),
    ('1018b356-c02d-4066-b9af-908b45a03f3a', '99ba2239-d61f-455c-9bf6-5eb14c42fa1b', 'https://www.imdb.com/title/tt1375666/mediaviewer/rm1010'),
    ('1018b358-c02d-4066-b9af-908b45a03f3a', '99ba2239-d61f-455c-9bf6-5eb14c42fa1b', 'https://www.imdb.com/title/tt1375666/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('99ba2239-d61f-455c-9bf6-5eb14c42fa1b', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('99ba2239-d61f-455c-9bf6-5eb14c42fa1b', 'd7b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('d7b9216e-21aa-4966-b006-388d0fc4dbcf', 'The Departed', 2006, 151, 'An undercover cop and a mole in the police attempt to identify each other while infiltrating an Irish gang in South Boston.');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('97844bfb-7849-4730-b7c2-1405e8338abe', 'd7b9216e-21aa-4966-b006-388d0fc4dbcf', 'https://www.imdb.com/title/tt0407887/mediaviewer/rm10105600'),
    ('97834bfb-7849-4730-b7c2-1405e8338abe', 'd7b9216e-21aa-4966-b006-388d0fc4dbcf', 'https://www.imdb.com/title/tt0407887/mediaviewer/rm1010'),
    ('97854bfb-7849-4730-b7c2-1405e8338abe', 'd7b9216e-21aa-4966-b006-388d0fc4dbcf', 'https://www.imdb.com/title/tt0407887/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('d7b9216e-21aa-4966-b006-388d0fc4dbcf', 'd1b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('d7b9216e-21aa-4966-b006-388d0fc4dbcf', 'd5b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('11009023-daa6-4842-b35a-5710f8f0f12f', 'The Prestige', 2006, 130, 'After a tragic accident, two stage magicians engage in a battle to create the ultimate illusion while sacrificing everything they have to outwit each other.');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('4fa9c198-eaf2-4fd8-86f6-0cf3bbb69453', '11009023-daa6-4842-b35a-5710f8f0f12f', 'https://www.imdb.com/title/tt0482571/mediaviewer/rm10105600'),
    ('4fa9c298-eaf2-4fd8-86f6-0cf3bbb69453', '11009023-daa6-4842-b35a-5710f8f0f12f', 'https://www.imdb.com/title/tt0482571/mediaviewer/rm1010'),
    ('4fa9c398-eaf2-4fd8-86f6-0cf3bbb69453', '11009023-daa6-4842-b35a-5710f8f0f12f', 'https://www.imdb.com/title/tt0482571/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('11009023-daa6-4842-b35a-5710f8f0f12f', 'd1b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('11009023-daa6-4842-b35a-5710f8f0f12f', 'd7b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

INSERT INTO movie (movie_id, title, year_of_release, duration, description) VALUES
    ('2c69ef78-e79b-444b-bd73-cf391f810bf7', 'The Green Mile', 1999, 189, 'The lives of guards on Death Row are affected by one of their charges');
INSERT INTO image (image_id, movie_id, url) VALUES
    ('c536035a-ea2c-4041-828b-076a44c979fd', '2c69ef78-e79b-444b-bd73-cf391f810bf7', 'https://www.imdb.com/title/tt0120689/mediaviewer/rm10105600'),
    ('c536135a-ea2c-4041-828b-076a44c979fd', '2c69ef78-e79b-444b-bd73-cf391f810bf7', 'https://www.imdb.com/title/tt0120689/mediaviewer/rm1010'),
    ('c536235a-ea2c-4041-828b-076a44c979fd', '2c69ef78-e79b-444b-bd73-cf391f810bf7', 'https://www.imdb.com/title/tt0120689/mediaviewer/rm1010500');
INSERT INTO movie_genre (movie_id, genres_genre_id) VALUES
    ('2c69ef78-e79b-444b-bd73-cf391f810bf7', 'd1b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b'),
    ('2c69ef78-e79b-444b-bd73-cf391f810bf7', 'd2b3b3b1-1b1b-4b1b-8b1b-1b1b1b1b1b1b');

COMMIT;