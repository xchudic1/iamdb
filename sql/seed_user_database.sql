--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2 (Debian 16.2-1.pgdg120+2)
-- Dumped by pg_dump version 16.2 (Debian 16.2-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: users; Type: TABLE; Schema: public; Owner: user_user
--

CREATE TABLE public.users (
                              id uuid NOT NULL,
                              email character varying(255) NOT NULL,
                              name character varying(255) NOT NULL,
                              role character varying(255) NOT NULL,
                              CONSTRAINT users_email_check CHECK (((email)::text ~* '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$'::text))
);


ALTER TABLE public.users OWNER TO user_user;

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: user_user
--

COPY public.users (id, email, name, role) FROM stdin;
b819d467-591a-4647-af40-8fbd9f986700	john.doe@example.com	John Doe	user
efd48184-5eee-4d5a-a923-ed7e2a3aecd6	jane.smith@example.com	Jane Smith	user
286cf664-5a78-455e-a423-1c2bdfa90129	aliyah.khan@example.com	Aliyah Khan	user
6daa41eb-c092-477d-b501-a301f7b9c3c8	benjamin.miller@example.com	Benjamin Miller	user
c5daccb3-0d1f-4471-bda3-7837020835bd	chloe.nguyen@example.com	Chloe Nguyen	user
a09e012a-749f-4b32-96ac-fe04c9aa3c6b	diego.hernandez@example.com	Diego Hernandez	user
cf2b0e4e-3af9-4afd-bea2-c3f20c349e72	emma.williams@example.com	Emma Williams	user
6df4fe75-6b51-48b4-b4f5-a294454b29e0	firoz.khan@example.com	Firoz Khan	user
281e9955-8421-4b82-af3a-0de4cbb5d4fb	gabriella.garcia@example.com	Gabriella Garcia	user
e15bac4c-fbc0-4edc-bac4-d7a370581286	henry.zhang@example.com	Henry Zhang	user
\.


--
-- Name: users idx_user_email; Type: CONSTRAINT; Schema: public; Owner: user_user
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT idx_user_email UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: user_user
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

