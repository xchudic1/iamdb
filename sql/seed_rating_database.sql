-- seed_rating_database.sql
BEGIN;

-- Clear existing data
TRUNCATE comments, ratings;

-- Insert new ratings data
INSERT INTO ratings (id_rating, movie_id, user_id, score, review) VALUES
                                                                      ('1cd25723-f25b-4f1c-9235-8cf84745be77', 'ae5f4109-3575-458f-88c4-e3e4946b06f7', '515fb1b1-9f88-4268-a64b-b0b3c000163a', 9.0, 'An absolute masterpiece.'),
                                                                      ('8c56be3f-dd79-4ff4-9807-2e1d1107d96c', 'b2ed63fe-02c7-4c3a-9ffb-ef29f93b2dcd', '68512f5a-6304-46a1-ac7f-b4ed7eec7d48', 8.5, 'Really enjoyed the visuals.'),
                                                                      ('0e00cfef-410d-4090-88e3-f6bb2ccdea22', '1326ad37-b42c-41c5-83d5-a54dc2994d42', 'eeeed3b5-94bb-4aca-8d35-371174b9a095', 7.5, 'Great story but slow at times.'),
                                                                      ('cff195c1-94c7-4b36-b8ba-a03a06bd4489', '4414e0ad-a947-44d0-ac5e-4fb132fe77d5', 'd38f76b5-10e4-482e-ad7a-8626923c6cbd', 6.0, 'Good effort, but lacking depth.'),
                                                                      ('e82da507-2177-4271-bebf-43b67d6483b6', 'a42eb540-afca-4ea7-98ad-b5dd5f39963e', '965a57a7-8bfa-46ec-ac79-1026be536f7e', 5.5, 'Mediocre at best.'),
                                                                      ('9aa39101-ea63-4d52-a460-b5501ee90ecb', 'daae78a8-4103-47c9-9f1b-b38303c7f47f', '19a8e571-ebec-45be-bdbe-5008a273b9b9', 4.5, 'It was somewhat disappointing.'),
                                                                      ('c536035a-ea2c-4041-828b-076a44c979fd', '237820f3-084e-4669-904d-ec72b35f24b2', 'fd8e5331-60c5-4708-8ae9-94aff7fb44dd', 3.5, 'Not my cup of tea.'),
                                                                      ('e53ee86e-1bb4-4400-a8a7-dd154aae3874', '575a250d-ce5b-436e-9d55-67a7b6f06e8c', '9eccf3db-c12c-408a-9df1-b042aa6051f7', 8.0, 'Engaging and fun to watch.'),
                                                                      ('1018b356-c02d-4066-b9af-908b45a03f3a', '1aff6dd1-f6ac-4745-93c2-b1e345023153', '99ba2239-d61f-455c-9bf6-5eb14c42fa1b', 7.0, 'Decent enough to watch once.'),
                                                                      ('97834bfb-7849-4730-b7c2-1405e8338abe', '2c69ef78-e79b-444b-bd73-cf391f810bf7', 'd7b9216e-21aa-4966-b006-388d0fc4dbcf', 8.7, 'Thoroughly entertaining.'),
                                                                      ('4fa9c198-eaf2-4fd8-86f6-0cf3bbb69453', '631f48b8-21c1-49ca-961c-48a1a616a3a1', '11009023-daa6-4842-b35a-5710f8f0f12f', 9.3, 'Brilliant acting and script.');

-- Insert new comments data
INSERT INTO comments (id_comment, rating_id, user_id, text) VALUES
                                                                ('001f9673-c5ad-4876-b0ea-4f97e5b5a18f', '1cd25723-f25b-4f1c-9235-8cf84745be77', '19a8e571-ebec-45be-bdbe-5008a273b9b9', 'Absolutely agree! A true masterpiece.'),
                                                                ('002fc4b2-045b-4852-85e6-8ebf53a8bfcf', '8c56be3f-dd79-4ff4-9807-2e1d1107d96c', 'fd8e5331-60c5-4708-8ae9-94aff7fb44dd', 'The visuals were stunning indeed.'),
                                                                ('003c8474-cfdd-49b5-9b0f-60b8e8ffcb5b', '0e00cfef-410d-4090-88e3-f6bb2ccdea22', '99ba2239-d61f-455c-9bf6-5eb14c42fa1b', 'I found the pace just right.'),
                                                                ('004d6233-3479-4f5c-9e70-53188c5a8e8f', 'cff195c1-94c7-4b36-b8ba-a03a06bd4489', 'd7b9216e-21aa-4966-b006-388d0fc4dbcf', 'Could have been better, but still good.'),
                                                                ('005e3c9b-bc50-4a1f-b4c8-31c81f2a4d33', 'e82da507-2177-4271-bebf-43b67d6483b6', '11009023-daa6-4842-b35a-5710f8f0f12f', 'It was okay, nothing special.'),
                                                                ('006f25c3-7d99-42b8-9c13-4c14e1c6d3b9', '9aa39101-ea63-4d52-a460-b5501ee90ecb', '515fb1b1-9f88-4268-a64b-b0b3c000163a', 'Agree, quite disappointing.'),
                                                                ('0070f91e-30b8-4988-a1d1-b1f8280f5d49', 'c536035a-ea2c-4041-828b-076a44c979fd', '965a57a7-8bfa-46ec-ac79-1026be536f7e', 'Not my favorite either.'),
                                                                ('00815cba-81cc-426a-8662-1f96b76f74d5', 'e53ee86e-1bb4-4400-a8a7-dd154aae3874', '9eccf3db-c12c-408a-9df1-b042aa6051f7', 'Fun movie, highly recommended.'),
                                                                ('00927f02-bc50-482a-b4c8-4fa31f81f2a4', '1018b356-c02d-4066-b9af-908b45a03f3a', '19a8e571-ebec-45be-bdbe-5008a273b9b9', 'Worth a watch.'),
                                                                ('00a34bc4-7d99-45e8-b1d1-1c13d0f5d3b9', '97834bfb-7849-4730-b7c2-1405e8338abe', '515fb1b1-9f88-4268-a64b-b0b3c000163a', 'Great entertainment.'),
                                                                ('00b467d6-81cc-432e-9262-5fa36b72d4a1', '4fa9c198-eaf2-4fd8-86f6-0cf3bbb69453', 'd7b9216e-21aa-4966-b006-388d0fc4dbcf', 'Amazing script and acting.');

COMMIT;
