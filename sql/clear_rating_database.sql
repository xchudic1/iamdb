-- Clear existing data in both ratings and comments tables
TRUNCATE TABLE comments RESTART IDENTITY CASCADE;
TRUNCATE TABLE ratings RESTART IDENTITY CASCADE;
