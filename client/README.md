# Client Project

This project provides the token to access the endpoints.

## Running

Execute the following command to run the project.

```shell
mvn clean install -DskipTests && mvn spring-boot:run
```

## Getting the token

- go to <http://localhost:8080>
- login with `muni`
- -> `yes continue` button
- click the `go to my calendar interface` button
- even though the backend is not running, the token is available at the console.

## Using the token

```shell
export TOKEN=the_token
```

- then using it in the requests eg.

```shell
curl -X GET http://localhost:8003/api/v1/test -H "Authorization: Bearer $TOKEN" -w "%{http_code}\n"
```

- (notice the format of the header)
