package cz.muni.pa165.oauth2.client;

import cz.muni.pa165.mycalendar.MyCalendarApi;
import cz.muni.pa165.mycalendar.invoker.ApiClient;
import cz.muni.pa165.mycalendar.invoker.ApiException;
import cz.muni.pa165.mycalendar.model.CalendarEvent;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.net.ConnectException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TreeSet;
import java.util.UUID;

/**
 * Spring MVC Controller.
 * Handles HTTP requests by preparing data in model and passing it to Thymeleaf HTML templates.
 */
@Controller
public class MainController {

    private static final Logger log = LoggerFactory.getLogger(MainController.class);

    /**
     * Home page accessible even to non-authenticated users. Displays user personal data.
     */
    @GetMapping("/")
    public String index(Model model, @AuthenticationPrincipal OidcUser user) {
        log.debug("********************************************************");
        log.debug("* index() called                                       *");
        log.debug("********************************************************");
        log.debug("user {}", user == null ? "is anonymous" : user.getSubject());

        // put obtained user data into a model attribute named "user"
        model.addAttribute("user", user);

        // put issuer name into a model attribute named "issuerName"
        if (user != null) {
            model.addAttribute("issuerName",
                    "https://oidc.muni.cz/oidc/".equals(user.getIssuer().toString()) ? "MUNI" : "Google");
        }

        // return the name of a Thymeleaf HTML template that
        // will be searched in src/main/resources/templates with .html suffix
        return "index";
    }


    /**
     * My Calendar page. Displays a list of calendar events obtained from Resource Server and
     * a form for creating a new event.
     *
     * @param eventForm object for default values of new event form fields
     * @param model the place for the data provided for the Thymeleaf template
     * @param user info about OIDC user
     * @param oauth2Client info about OAuth2 client and access token
     * @return template name
     */
    @GetMapping("/mycalendar")
    public String myCalendar(Model model,
                             @ModelAttribute EventForm eventForm,
                             @AuthenticationPrincipal OidcUser user,
                             @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {
        log.debug("********************************************************");
        log.debug("* myCalendar() called                                  *");
        log.debug("********************************************************");

        OAuth2AccessToken accessToken = oauth2Client.getAccessToken();
        // log access token
        log.debug("access token principal: {}", oauth2Client.getPrincipalName());
        log.debug("access token scopes: {}", accessToken.getScopes());
        log.debug("access token issued: {}", accessToken.getIssuedAt().atZone(ZoneId.systemDefault()));
        log.debug("access token expires: {}", accessToken.getExpiresAt().atZone(ZoneId.systemDefault()));
        log.debug("access token value: {}", accessToken.getTokenValue());

        // add access token to API calls
        ApiClient apiClient = new ApiClient();
        apiClient.setRequestInterceptor(httpRequestBuilder -> {
            httpRequestBuilder.header("Authorization", "Bearer " + accessToken.getTokenValue());
        });

        // call My Calendar API
        MyCalendarApi myCalendarApi = new MyCalendarApi(apiClient);
        try {
            List<CalendarEvent> myEvents = myCalendarApi.getUserEvents();
            log.debug("API getUserEvents successfully called");
            model.addAttribute("events", myEvents);
        } catch (ApiException apiException) {
            handleApiException(model, apiException);
        }

        // for display in heading
        model.addAttribute("user", user);
        model.addAttribute("scopes", new TreeSet<>(accessToken.getScopes()));

        // default times for form
        eventForm.setStartDate(LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE));
        eventForm.setEndDate(LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE));
        eventForm.setStartTime(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        eventForm.setEndTime(LocalTime.now().plusHours(1).format(DateTimeFormatter.ofPattern("HH:mm")));
        eventForm.setTitle("event");

        // return Thymeleaf HTML template name
        return "mycalendar";
    }

    /**
     * Receives submitted HTML form with a new calendar event.
     *
     * @param eventForm receives values from the HTML form
     * @param bindingResult result of binding the form field values to the EventForm instance properties
     * @param model the place for the data provided for the Thymeleaf template
     * @param user info about OIDC user
     * @param oauth2Client info about OAuth2 client and access token
     * @return template name
     */
    @PostMapping("mycalendar")
    public String createEvent(@Valid @ModelAttribute EventForm eventForm,
                              BindingResult bindingResult,
                              Model model,
                              @AuthenticationPrincipal OidcUser user,
                              @RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient oauth2Client) {
        log.debug("********************************************************");
        log.debug("* createEvent() called                                 *");
        log.debug("********************************************************");

        // for displaying user and scopes in heading
        OAuth2AccessToken accessToken = oauth2Client.getAccessToken();
        model.addAttribute("user", user);
        model.addAttribute("scopes", accessToken.getScopes());

        // add access token to My Calendar API calls
        ApiClient apiClient = new ApiClient();
        apiClient.setRequestInterceptor(httpRequestBuilder -> {
            httpRequestBuilder.header("Authorization", "Bearer " + accessToken.getTokenValue());
        });
        MyCalendarApi myCalendarApi = new MyCalendarApi(apiClient);

        //check binding errors (EventForm was not valid according to its annotations)
        if (bindingResult.hasErrors()) {
            // get list of events for display
            try {
                List<CalendarEvent> myEvents = myCalendarApi.getUserEvents();
                log.debug("API getUserEvents successfully called");
                model.addAttribute("events", myEvents);
            } catch (ApiException apiException) {
                handleApiException(model, apiException);
            }
            return "mycalendar";
        }
        // create new event
        try {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            myCalendarApi.createEvent(new CalendarEvent()
                    .id(UUID.randomUUID().toString())
                    .title(eventForm.getTitle())
                    .description(eventForm.getDescription())
                    .location(eventForm.getLocation())
                    .startTime(df
                            .parse(eventForm.getStartDate() + " " + eventForm.getStartTime(), LocalDateTime::from)
                            .atZone(ZoneId.systemDefault()).toOffsetDateTime())
                    .endTime(df
                            .parse(eventForm.getEndDate() + " " + eventForm.getEndTime(), LocalDateTime::from)
                            .atZone(ZoneId.systemDefault()).toOffsetDateTime())
            );
            log.debug("API createEvent successfully called");
        } catch (ApiException apiException) {
            handleApiException(model, apiException);
            return "mycalendar";
        }

        // redirect-after-post to clear the post from browser history
        return "redirect:/mycalendar";
    }

    private void handleApiException(Model model, ApiException apiException) {
        Throwable cause = apiException.getCause();
        if (cause instanceof ConnectException) {
            log.error("cannot call API, resource server is down");
            model.addAttribute("connect_problem", true);
        } else if (cause != null) {
            log.error("cannot call API", cause);
            model.addAttribute("exception", cause);
        } else if (apiException.getCode() == HttpStatus.FORBIDDEN.value()) {
            log.error("API returns HTTP status FORBIDDEN");
            model.addAttribute("api_forbidden", true);
            model.addAttribute("wwwAuthenticate", apiException.getResponseHeaders()
                    .firstValue("www-authenticate").orElse(null));
        } else {
            log.error("API returns an error: " + apiException.getMessage());
            model.addAttribute("api_problem", true);
            model.addAttribute("api_exception", apiException);
        }
    }


}