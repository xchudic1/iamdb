# Movie Microservice

The Movie Microservice is designed to manage list of movies, the Genres and the Images of each movie. It provides CRUD operations and aggregation of three entities Movie, Genre and Image. 

## Getting Started

To get the microservice running you need to run docker compose in the parent directory. This will start the required services (database) for the microservice to run.

```bash
docker compose up --build 
```

The movie microservice endpoints work in [localhost:8001/api/v1/movies](http://localhost:8001/api/v1/movies).

## Movie API endpoints

This section details the available API endpoints for the Movie managing in Movie Microservice, including the expected request bodies and the responses for each operation.

### Create a Movie
**Endpoint**: POST /api/v1/microservice-movie/movies

**Description**: Allows an admin to create a new movie.

**Request Body Example**:
```bash
{
"title": "Your Movie Title",
"year_of_release": 2021,
"genre_ids": [],
"duration": 120,
"description": "A brief description of the movie"
}
```

**Response**: Returns 200 OK along with newly created movie with _Id_ automatically set.

```bash
{
    "title": "Your Movie Title",
    "year_of_release": 2021,
    "genres": [],
    "images": [],
    "duration": 120,
    "description": "A brief description of the movie"
}
```
### Update a Movie
**Endpoint**: PUT /api/v1/microservice-movie/movies/{id}

**Description**: Updates an existing movie. The {id} in the URL is the ID of the movie to update.

**Request Body Example**: PUT /api/v1/microservice-movie/movies/89df89af-6bb6-48d5-b19a-6b60a1627ba2
```bash
{
"title": "New title",
"year_of_release": 2023,
"genre_ids": [],
"duration": 120,
"description": "A brief description of the movie"
}
```
**Response**: Returns 200 OK along with updated Movie.
```Bash
{
"id": 89df89af-6bb6-48d5-b19a-6b60a1627ba2,
"title": "New title",
"year_of_release": 2023,
"genres": [],
"images": [],
"duration": 120,
"description": "A brief description of the movie"
}
```

### Delete a Movie
**Endpoint**: DELETE /api/v1/movie-microservice/movies/{id}

**Description**: Deletes a specific movie by its ID.

**Response**: A successful operation will return a _204 No Content_ status.

### Find a Specific Movie
**Endpoint**: GET /api/v1/movie-microservice/movies/{id}

**Description**: Fetches details of a specific rating by its ID.

**Response**: Returns the requested Movie object.

### Find All Movie

**Endpoint**: GET /api/v1/movie-microservice/movies

**Description**: Retrieves a list of all movies in the system.

**Response**: Returns a list of Movie objects.

You can also filter the movies based on the genre ID passed in query parameters.
For example:

```bash
GET /api/v1/movie-microservice/movies?genreId={id}
```

### Find Images for specific Movie

**Endpoint**: GET /api/v1/movie-microservice/movies/{id}/images

**Description**: Retrieves a list of all images that belongs to the movie.

**Response**: Returns a list of Movie objects.

## Genre API endpoints

```Bash
/api/v1/microservice-movie/genres
```
Same implementation as Movie API endpoints except endpoint [Find Images for specific Movie](#find-images-for-specific-movie)

## Image API endpoints

```Bash
/api/v1/microservice-movie/images
```
Same implementation as Movie API endpoints except endpoint [Find Images for specific Movie](#find-images-for-specific-movie)

## Seeding and clearing

To seed the database with some initial data, you can use the following command while running the app:
```bash
docker cp ./sql/seed_movie_database.sql movie-postgres:/ && \
docker exec -it movie-postgres psql -U movie_user -d movie_database -p 5431 -a -f ./seed_movie_database.sql
```
To clear the whole database, you can use the following script:
```bash
docker cp ./sql/clear_movie_database.sql movie-postgres:/ && \
docker exec -it movie-postgres psql -U movie_user -d movie_database -p 5431 -a -f ./clear_movie_database.sql
```

For windows use `;` instead of `&&` in the above commands.


## Validation and Error handling

DTOs are validate upon creation and violating any of the validator will result in error handled by GlobalExceptionHandler.
Response is 400 BAD_REQUEST.

```Bash
{
    "timestamp": "2024-04-02T15:11:17.2110185",
    "status": "BAD_REQUEST",
    "message": "[Title must not be blank]",
    "path": "/api/v1/movies/3"
}
```

Responses to unsuccessful requests will include an appropriate HTTP status code and JSON object with details.

```bash
{
    "timestamp": "2024-04-02T15:13:25.9633931",
    "status": "NOT_FOUND",
    "message": "Movie with id: 89df89af-6bb6-48d5-b19a-6b60a1627ba2 was not found.",
    "path": "/api/v1/movies/89df89af-6bb6-48d5-b19a-6b60a1627ba2"
}
```

## Testing 

You can use swagger with url <http://localhost:8001/api/v1/swagger-ui/index.html#/> to test the endpoints.

- (your host, port may be different depending on your env)







