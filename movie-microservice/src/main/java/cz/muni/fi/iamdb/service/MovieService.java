package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Movie;
import cz.muni.fi.iamdb.data.repository.MovieRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class MovieService {
    private final MovieRepository movieRepository;


    @Autowired
    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }
    @Transactional(readOnly = true)
    public Movie findById(UUID id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Movie with id: " + id + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<Movie> findAll(Optional<UUID> genreId) {
        return movieRepository.findAllByCriteria(genreId);
    }

    @Transactional
    public Movie createMovie(Movie movie){
        if (movie == null) {
            throw new IllegalArgumentException("Movie must be null when creating a new movie.");
        }
        return movieRepository.save(movie);
    }

    @Transactional
    public Movie updateMovie(UUID id, Movie movie){
        if (!movieRepository.existsById(id)) {
            throw new ResourceNotFoundException("Movie with ID " + id + " does not exist to update.");
        }
        Movie existingMovie = findById(id);
        existingMovie.setGenres(movie.getGenres());
        existingMovie.setDescription(movie.getDescription());
        existingMovie.setTitle(movie.getTitle());
        existingMovie.setYearOfRelease(movie.getYearOfRelease());
        existingMovie.setImages(movie.getImages());
        existingMovie.setDuration(movie.getDuration());
        return movieRepository.save(existingMovie);
    }

    @Transactional
    public void deleteMovie(UUID id){
        if (!movieRepository.existsById(id)) {
            throw new ResourceNotFoundException("Movie with ID " + id + " does not exist.");
        }
        movieRepository.deleteById(id);
    }
}
