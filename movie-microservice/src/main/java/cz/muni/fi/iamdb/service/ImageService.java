package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Image;
import cz.muni.fi.iamdb.data.repository.ImageRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class ImageService {
    private final ImageRepository imageRepository;

    @Autowired
    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Transactional(readOnly = true)
    public Image findById(UUID id) {
        return imageRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Image with id: " + id + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<Image> findAll(){
        return imageRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Image> findAllById(List<UUID> ids){
        return imageRepository.findAllById(ids);
    }

    @Transactional(readOnly = true)
    public List<Image> findImagesForMovie(UUID movieId) {
        return imageRepository.findByMovieId(movieId);
    }

    @Transactional
    public Image createImage(Image image){
        if (image == null) {
            throw new IllegalArgumentException("Image must be null when creating a new image.");
        }
        return imageRepository.save(image);
    }
    @Transactional
    public Image updateImage(UUID id, Image image){
        if (!imageRepository.existsById(id)) {
            throw new ResourceNotFoundException("Image with ID " + id + " does not exist to update.");
        }
        Image existingImage = findById(id);
        existingImage.setMovie(image.getMovie());
        existingImage.setUrl(image.getUrl());
        existingImage.setDescription(image.getDescription());
        existingImage.setId(id);
        return imageRepository.save(existingImage);
    }

    @Transactional
    public void deleteImage(UUID id){
        if (!imageRepository.existsById(id)) {
            throw new ResourceNotFoundException("Image with ID " + id + " does not exist.");
        }
        imageRepository.deleteById(id);
    }
}
