package cz.muni.fi.iamdb.mappers;

import cz.muni.fi.iamdb.api.GenreDto;
import cz.muni.fi.iamdb.data.model.Genre;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GenreMapper {
    GenreDto mapToDto(Genre genre);

    List<GenreDto> mapToList(List<Genre> genre);

    @Mapping(target = "movies", ignore = true)
    Genre mapToEntity(GenreDto genreDto);
}
