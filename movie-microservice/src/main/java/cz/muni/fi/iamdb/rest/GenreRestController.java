package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.GenreDto;
import cz.muni.fi.iamdb.facade.GenreFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/movies/genres")
public class GenreRestController {
    private final GenreFacade genreFacade;

    @Autowired
    public GenreRestController(GenreFacade genreFacade) {
        this.genreFacade = genreFacade;
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find Genre by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Genre found"),
            @ApiResponse(responseCode = "404", description = "Genre not found")
    })
    public ResponseEntity<GenreDto> findById(
            @Parameter(description = "ID of Genre to be searched") @PathVariable("id") UUID id) {
        return ResponseEntity.ok(genreFacade.findById(id));
    }

    @GetMapping(path = "")
    @Operation(summary = "Find all Genres")
    @ApiResponse(responseCode = "200", description = "List of Genres found")
    public ResponseEntity<List<GenreDto>> findAll() {
        return ResponseEntity.ok(genreFacade.findAll());
    }

    @PostMapping(path = "")
    @Operation(summary = "Create Genre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Genre created"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Genre already exists")
    })
    public ResponseEntity<GenreDto> createGenre(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Genre to be created") @RequestBody GenreDto genreDto){
        GenreDto createdGenre = genreFacade.createGenre(genreDto);
        return new ResponseEntity<>(createdGenre, HttpStatus.CREATED);
    }

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update Genre by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Genre updated"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Genre not found")
    })
    public ResponseEntity<GenreDto> updateGenre(
            @Parameter(description = "ID of Genre to be updated") @PathVariable("id") UUID id,
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Genre to be updated") @RequestBody GenreDto genreDto){
        return ResponseEntity.ok(genreFacade.updateGenre(id, genreDto));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete Genre by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Genre deleted"),
            @ApiResponse(responseCode = "404", description = "Genre not found")
    })
    public void deleteGenre(
            @Parameter(description = "ID of Genre to be deleted") @PathVariable("id") UUID id){
        genreFacade.deleteGenre(id);
    }
}

