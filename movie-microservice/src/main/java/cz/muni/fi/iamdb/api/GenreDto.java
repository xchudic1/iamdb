package cz.muni.fi.iamdb.api;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public class GenreDto {
        private UUID id;
        @NotBlank(message = "Name must not be blank")
        @NotNull
        private String name;

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public UUID getId() {
                return id;
        }

        public void setId(UUID id) {
                this.id = id;
        }
}
