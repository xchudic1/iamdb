package cz.muni.fi.iamdb.mappers;

import cz.muni.fi.iamdb.api.ImageDto;
import cz.muni.fi.iamdb.data.model.Image;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImageMapper {

    ImageDto mapToDto(Image image);

    List<ImageDto> mapToList(List<Image> images);

    @Mapping(target="movie", ignore = true)
    Image mapToEntity(ImageDto imageDto);
}
