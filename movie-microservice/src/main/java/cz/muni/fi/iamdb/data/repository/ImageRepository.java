package cz.muni.fi.iamdb.data.repository;

import cz.muni.fi.iamdb.data.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ImageRepository extends JpaRepository<Image, UUID> {
    @Query("SELECT i FROM Image i WHERE i.movie.id = :id")
    List<Image> findByMovieId(UUID id);

}
