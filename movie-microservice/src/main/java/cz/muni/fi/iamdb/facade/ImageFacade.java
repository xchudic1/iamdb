package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.ImageDto;
import cz.muni.fi.iamdb.data.model.Image;
import cz.muni.fi.iamdb.mappers.ImageMapper;
import cz.muni.fi.iamdb.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class ImageFacade {
    private final ImageService imageService;

    private final ImageMapper imageMapper;

    @Autowired
    public ImageFacade(ImageService imageService, ImageMapper imageMapper) {
        this.imageService = imageService;
        this.imageMapper = imageMapper;
    }

    public ImageDto findById(UUID id) {
        return imageMapper.mapToDto(imageService.findById(id));
    }

    public List<ImageDto> findAll(){
        return imageMapper.mapToList(imageService.findAll());
    }

    public ImageDto createImage(ImageDto imageDto){
        Image image = imageMapper.mapToEntity(imageDto);
        Image createdImage = imageService.createImage(image);
        return imageMapper.mapToDto(createdImage);
    }

    public ImageDto updateImage(UUID id, ImageDto imageDto){
        Image image = imageMapper.mapToEntity(imageDto);
        Image updatedImage = imageService.updateImage(id, image);
        return imageMapper.mapToDto(updatedImage);
    }

    public void deleteImage(UUID id){
        imageService.deleteImage(id);
    }
}
