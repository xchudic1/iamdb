package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.ImageDto;
import cz.muni.fi.iamdb.api.MovieBasicViewDto;
import cz.muni.fi.iamdb.api.MovieCreateDto;
import cz.muni.fi.iamdb.api.MovieDetailedViewDto;
import cz.muni.fi.iamdb.facade.MovieFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/movies")
public class MovieRestController {
    private final MovieFacade movieFacade;

    @Autowired
    public MovieRestController(MovieFacade movieFacade) {
        this.movieFacade = movieFacade;
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find movie by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Movie found"),
            @ApiResponse(responseCode = "404", description = "Movie not found")
    })
    public ResponseEntity<MovieDetailedViewDto> findById(@Parameter(description = "ID of movie to be searched") @PathVariable("id") UUID id) {
        return ResponseEntity.ok(movieFacade.findById(id));
    }

    @GetMapping(path = "")
    @Operation(summary = "Find all movies")
    @ApiResponse(responseCode = "200", description = "List of movies found")
    public ResponseEntity<List<MovieBasicViewDto>> findAll(@RequestParam Optional<UUID> genreId) {
        return ResponseEntity.ok(movieFacade.findAll(genreId));
    }

    @GetMapping(path="/{id}/images")
    public ResponseEntity<List<ImageDto>> showMovieImages(@PathVariable("id") UUID id){
        return ResponseEntity.ok(movieFacade.findImagesForMovie(id));
    }
    @PostMapping(path = "")
    @Operation(summary = "Create Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Movie created"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Movie already exists")
    })
    public ResponseEntity<MovieDetailedViewDto> createMovie(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Movie to be created") @Valid @RequestBody MovieCreateDto movieCreateDto){
        MovieDetailedViewDto createdMovie = movieFacade.createMovie(movieCreateDto);
        return new ResponseEntity<>(createdMovie, HttpStatus.CREATED);
    }

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update movie by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Movie updated"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Movie not found")
    })
    public ResponseEntity<MovieDetailedViewDto> updateMovie(
            @Parameter(description = "ID of movie to be updated") @PathVariable("id") UUID id,
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Movie to be updated") @Valid @RequestBody MovieCreateDto movieCreateDto){
        return ResponseEntity.ok(movieFacade.updateMovie(id, movieCreateDto));
    }


    // done
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete movie by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Movie deleted"),
            @ApiResponse(responseCode = "404", description = "Movie not found")
    })
    public void deleteMovie(
            @Parameter(description = "ID of movie to be deleted") @PathVariable("id") UUID id){
        movieFacade.delete(id);
    }

}
