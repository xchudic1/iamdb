package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Genre;
import cz.muni.fi.iamdb.data.repository.GenreRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class GenreService {
    private final GenreRepository genreRepository;
    @Autowired
    public GenreService(GenreRepository genreRepository){
        this.genreRepository = genreRepository;
    }

    @Transactional(readOnly = true)
    public Genre findById(UUID id){
        return genreRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Genre with id: " + id + "was not found."));
    }

    @Transactional(readOnly = true)
    public List<Genre> findAll(){
        return genreRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Genre> findAllById(List<UUID> ids){
        return genreRepository.findAllById(ids);
    }
    @Transactional
    public Genre createGenre(Genre genre){
        if (genre == null) {
            throw new IllegalArgumentException("Genre must be null when creating a new genre.");
        }
        return genreRepository.save(genre);
    }

    @Transactional
    public Genre updateGenre(UUID id, Genre genre){
        if (!genreRepository.existsById(id)) {
            throw new ResourceNotFoundException("Genre with ID " + id + " does not exist to update.");
        }
        Genre existingGenre = findById(id);
        existingGenre.setName(genre.getName());
        return genreRepository.save(existingGenre);
    }

    @Transactional
    public void deleteGenre(UUID id){
        if (!genreRepository.existsById(id)) {
            throw new ResourceNotFoundException("Genre with ID " + id + " does not exist.");
        }
        genreRepository.deleteById(id);
    }
}
