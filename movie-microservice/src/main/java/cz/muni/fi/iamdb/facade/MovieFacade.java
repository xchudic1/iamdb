package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.ImageDto;
import cz.muni.fi.iamdb.api.MovieBasicViewDto;
import cz.muni.fi.iamdb.api.MovieCreateDto;
import cz.muni.fi.iamdb.api.MovieDetailedViewDto;
import cz.muni.fi.iamdb.data.model.Genre;
import cz.muni.fi.iamdb.data.model.Image;
import cz.muni.fi.iamdb.data.model.Movie;
import cz.muni.fi.iamdb.mappers.ImageMapper;
import cz.muni.fi.iamdb.mappers.MovieMapper;
import cz.muni.fi.iamdb.service.GenreService;
import cz.muni.fi.iamdb.service.ImageService;
import cz.muni.fi.iamdb.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class MovieFacade {
    private final MovieService movieService;
    private final GenreService genreService;

    private final ImageService imageService;

    private final MovieMapper movieMapper;

    private final ImageMapper imageMapper;


    @Autowired
    public MovieFacade(MovieService movieService, GenreService genreService, ImageService imageService, MovieMapper movieMapper, ImageMapper imageMapper) {
        this.movieService = movieService;
        this.genreService = genreService;
        this.imageService = imageService;
        this.movieMapper = movieMapper;
        this.imageMapper = imageMapper;
    }

    public MovieDetailedViewDto findById(UUID id) {
        return movieMapper.mapToDetailViewDto(movieService.findById(id));
    }

    public List<MovieBasicViewDto> findAll(Optional<UUID> genreId) {
        return movieMapper.mapToList(movieService.findAll(genreId));
    }

    public List<ImageDto> findImagesForMovie(UUID id){
        return imageMapper.mapToList(imageService.findImagesForMovie(id));
    }

    public MovieDetailedViewDto createMovie(MovieCreateDto movieCreateDto){
        Movie movie = movieMapper.mapToEntity(movieCreateDto);

        try {
            movie.setGenres(resolveGenres(movieCreateDto.getGenreIds()));
            movie.setImages(resolveImages(movieCreateDto.getImageIds(), movie));
        }catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid genre or image id");
        }

        Movie createdMovie = movieService.createMovie(movie);
        return movieMapper.mapToDetailViewDto(createdMovie);
    }

    public MovieDetailedViewDto updateMovie(UUID id, MovieCreateDto movieCreateDto){
        Movie movie = movieMapper.mapToEntity(movieCreateDto);
        Movie updatedMovie = movieService.updateMovie(id, movie);
        return movieMapper.mapToDetailViewDto(updatedMovie);
    }

    public void delete(UUID id){
        movieService.deleteMovie(id);
    }

    private Set<Genre> resolveGenres(UUID[] genreIds) {
        if (genreIds == null || genreIds.length == 0)
            return new HashSet<>();
        List<Genre> genres = genreService.findAllById(Arrays.asList(genreIds));
        if (genres.size() != genreIds.length)
            throw new IllegalArgumentException("Not all genres were found");
        return new HashSet<>();
    }

    private Set<Image> resolveImages(UUID[] imageIds, Movie movie) {
        if (imageIds == null || imageIds.length == 0)
            return new HashSet<>();
        List<Image> images = imageService.findAllById(Arrays.asList(imageIds));
        if (images.size() != imageIds.length)
            throw new IllegalArgumentException("Not all images were found");
        for (Image image : images) {
            image.setMovie(movie);
        }
        return new HashSet<>(images);
    }
}
