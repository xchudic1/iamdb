package cz.muni.fi.iamdb.data.repository;

import cz.muni.fi.iamdb.data.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MovieRepository extends JpaRepository<Movie, UUID> {

    @Query("SELECT DISTINCT m FROM Movie m LEFT JOIN FETCH m.images i LEFT JOIN FETCH m.genres g WHERE (:genreId is null OR :genreId = g.id)")
    List<Movie> findAllByCriteria(@Param("genreId") Optional<UUID> genreId);

}
