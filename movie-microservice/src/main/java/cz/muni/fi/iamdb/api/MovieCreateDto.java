package cz.muni.fi.iamdb.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.*;

import java.util.Arrays;
import java.util.UUID;

public class MovieCreateDto
{
        @NotBlank(message = "Title must not be blank")
        @NotNull
        private String title;

        @JsonProperty("year_of_release")
        @Min(value = 1888, message = "Year of release must be greater than 1888")
        @NotNull
        private int yearOfRelease;

        @JsonProperty("genre_ids")
        private UUID[] genreIds;

        @JsonProperty("image_ids")
        private UUID[] imageIds;
        @Positive(message = "Duration must be positive")
        @NotNull
        private int duration;
        @NotBlank(message = "Description must not be blank")
        @NotNull
        private String description;

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public int getDuration() {
                return duration;
        }

        public void setDuration(int duration) {
                this.duration = duration;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public int getYearOfRelease() {
                return yearOfRelease;
        }

        public void setYearOfRelease(int yearOfRelease) {
                this.yearOfRelease = yearOfRelease;
        }

        public UUID[] getGenreIds() {
                return genreIds;
        }

        public void setGenreIds(UUID[] genreIds) {
                this.genreIds = genreIds;
        }

        public UUID[] getImageIds() {
                return imageIds;
        }

        @Override
        public String toString() {
                return "MovieCreateDto{" +
                        "title='" + title + '\'' +
                        ", yearOfRelease=" + yearOfRelease +
                        ", genreIds=" + Arrays.toString(genreIds) +
                        ", imageIds=" + Arrays.toString(imageIds) +
                        ", duration=" + duration +
                        ", description='" + description + '\'' +
                        '}';
        }
}
