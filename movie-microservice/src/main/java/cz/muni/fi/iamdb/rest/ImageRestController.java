package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.ImageDto;
import cz.muni.fi.iamdb.facade.ImageFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/movies/images")
public class ImageRestController {
    private final ImageFacade imageFacade;

    @Autowired
    public ImageRestController(ImageFacade imageFacade) {
        this.imageFacade = imageFacade;
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Find Image by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image found"),
            @ApiResponse(responseCode = "404", description = "Image not found")
    })
    public ResponseEntity<ImageDto> findById(
            @Parameter(description = "ID of user to be searched") @PathVariable("id") UUID id) {
        return ResponseEntity.ok(imageFacade.findById(id));
    }

    @GetMapping(path = "")
    @ApiResponse(responseCode = "200", description = "List of Images found")
    public ResponseEntity<List<ImageDto>> findAll() {
        return ResponseEntity.ok(imageFacade.findAll());
    }

    @PostMapping(path = "")
    @Operation(summary = "Create Image")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Image created"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Image already exists")
    })
    public ResponseEntity<ImageDto> createImage(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Image to be created") @RequestBody ImageDto imageDto){
        ImageDto createdImage = imageFacade.createImage(imageDto);
        return new ResponseEntity<>(createdImage, HttpStatus.CREATED);
    }

    @PutMapping(path = "/{id}")
    @Operation(summary = "Update Image by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Image updated"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Image not found")
    })
    public ResponseEntity<ImageDto> updateImage(
            @Parameter(description = "ID of Image to be updated") @PathVariable("id") UUID id,
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Image to be updated") @RequestBody ImageDto imageDto){
        return ResponseEntity.ok(imageFacade.updateImage(id, imageDto));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete Image by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Image deleted"),
            @ApiResponse(responseCode = "404", description = "Image not found")
    })
    public void deleteImage(@Parameter(description = "ID of Image to be deleted") @PathVariable("id") UUID id){
        imageFacade.deleteImage(id);
    }
}
