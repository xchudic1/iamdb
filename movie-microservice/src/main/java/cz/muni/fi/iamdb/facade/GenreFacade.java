package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.GenreDto;
import cz.muni.fi.iamdb.data.model.Genre;
import cz.muni.fi.iamdb.mappers.GenreMapper;
import cz.muni.fi.iamdb.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class GenreFacade {
    private final GenreService genreService;
    
    private final GenreMapper genreMapper;

    @Autowired
    public GenreFacade(GenreService genreService, GenreMapper genreMapper) {
        this.genreService = genreService;
        this.genreMapper = genreMapper;
    }

    @Transactional(readOnly = true)
    public GenreDto findById(UUID id) {
        return genreMapper.mapToDto(genreService.findById(id));
    }

    @Transactional(readOnly = true)
    public List<GenreDto> findAll(){
        return genreMapper.mapToList(genreService.findAll());
    }
    @Transactional
    public GenreDto createGenre(GenreDto genreDto){
        Genre genre = genreMapper.mapToEntity(genreDto);
        Genre createdGenre = genreService.createGenre(genre);
        return genreMapper.mapToDto(createdGenre);
    }

    @Transactional
    public GenreDto updateGenre(UUID id, GenreDto genreDto){
        Genre genre = genreMapper.mapToEntity(genreDto);
        Genre updatedGenre = genreService.updateGenre(id, genre);
        return genreMapper.mapToDto(updatedGenre);
    }

    @Transactional
    public void deleteGenre(UUID id){
        genreService.deleteGenre(id);
    }
    
}
