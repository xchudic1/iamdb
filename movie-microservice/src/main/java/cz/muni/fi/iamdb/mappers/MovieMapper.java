package cz.muni.fi.iamdb.mappers;

import cz.muni.fi.iamdb.api.MovieBasicViewDto;
import cz.muni.fi.iamdb.api.MovieCreateDto;
import cz.muni.fi.iamdb.api.MovieDetailedViewDto;
import cz.muni.fi.iamdb.data.model.Movie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MovieMapper {
    MovieBasicViewDto mapToDto(Movie movie);

    List<MovieBasicViewDto> mapToList(List<Movie> movies);

    @Mapping(target = "id", ignore = true)
    @Mapping(target="genres", ignore = true)
    @Mapping(target="images", ignore = true)
    Movie mapToEntity(MovieCreateDto movieCreateDto);

    MovieDetailedViewDto mapToDetailViewDto(Movie movie);

}
