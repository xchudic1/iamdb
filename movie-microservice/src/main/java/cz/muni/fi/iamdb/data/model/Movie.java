package cz.muni.fi.iamdb.data.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

//Persistance will be implemented in M2
@Entity
@Table(name = "movie")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "movie_id", nullable = false)
    private UUID id;
    @NotNull
    @Column(name = "title", length = 50, nullable = false)
    private String title;

    @NotNull
    @Column(name = "year_of_release", nullable = false)
    private int yearOfRelease;

    @NotNull
    @Column(name = "duration", nullable = false)
    private int duration;

    @Column(name = "description", length = 200)
    private String description;

    @JsonManagedReference
    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
    @JoinTable(
            name = "movie_genre",
            joinColumns = @JoinColumn(name = "movie_id")
    )
    private Set<Genre> genres = new HashSet<>();

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "movie",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Image> images;

    public Movie(){
    }

    public Movie(String title, int yearOfRelease, int duration, String description){
        this.title = title;
        this.yearOfRelease = yearOfRelease;
        this.duration = duration;
        this.description = description;
    }
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie movie)) return false;
        return Objects.equals(getId(), movie.getId());

    }

    @Override
    public String toString() {
        return "Title: " + this.title + "\n"
                + "Year : " + this.yearOfRelease + "\n"
                + "Duration: " + this.duration + "\n"
                + "Description: " + this.description;
    }


    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    public int getYearOfRelease(){
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease){
        this.yearOfRelease = yearOfRelease;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }
}
