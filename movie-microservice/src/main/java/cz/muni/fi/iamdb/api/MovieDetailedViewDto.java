package cz.muni.fi.iamdb.api;

import java.util.UUID;

public record MovieDetailedViewDto(
        UUID id,
        String title,
        int yearOfRelease,
        GenreDto[] genres,
        ImageDto[] images,
        int duration,
        String description
) {
}
