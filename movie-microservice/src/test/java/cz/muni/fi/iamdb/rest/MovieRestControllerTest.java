package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.*;
import cz.muni.fi.iamdb.data.model.Genre;
import cz.muni.fi.iamdb.facade.MovieFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
final class MovieRestControllerTest {
    @Mock
    private MovieFacade movieFacade;

    @InjectMocks
    private MovieRestController movieRestController;

    private MovieDetailedViewDto movieDetailedViewDto;

    private MovieCreateDto movieCreateDto;

    private Genre genre = new Genre();
    @BeforeEach
    void setUp(){
        UUID id = UUID.randomUUID();
        genre.setName("Comedy");
        movieDetailedViewDto = new MovieDetailedViewDto(id, "Test", 1999, new GenreDto[0], new ImageDto[0], 20, "desc");

        movieCreateDto = new MovieCreateDto();
        movieCreateDto.setDuration(20);
        movieCreateDto.setDescription("desc");
        movieCreateDto.setTitle("Test");
        movieCreateDto.setYearOfRelease(1999);
        movieCreateDto.setGenreIds(new UUID[1]);
    }

    @Test
    void  findById_validRequestBody_returnsResponseWithMovie(){
        UUID id = UUID.randomUUID();
        Mockito.when(movieFacade.findById(id)).thenReturn(movieDetailedViewDto);

        ResponseEntity<MovieDetailedViewDto> foundMovie = movieRestController.findById(id);

        assertThat(foundMovie.getBody()).isNotNull();
        assertThat(foundMovie.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(foundMovie.getBody().duration()).isEqualTo(20);
    }

    @Test
    void  findAll_validRequestBody_returnsResponseWithMovies(){
        UUID id = UUID.randomUUID();
        MovieBasicViewDto movieBasicViewDto = new MovieBasicViewDto(id, "Test", 20, 1999, new GenreDto[1]);
        Mockito.when(movieFacade.findAll(Optional.empty())).thenReturn(List.of(movieBasicViewDto));

        ResponseEntity<List<MovieBasicViewDto>> foundMovies = movieRestController.findAll(Optional.empty());

        assertThat(foundMovies.getBody()).isNotNull();
        assertThat(foundMovies.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
    }

    @Test
    void createMovie_validRequestBody_returnsResponseWithMovie(){
        Mockito.when(movieFacade.createMovie(movieCreateDto)).thenReturn(movieDetailedViewDto);

        ResponseEntity<MovieDetailedViewDto> foundMovie = movieRestController.createMovie(movieCreateDto);

        assertThat(foundMovie.getBody()).isNotNull();
        assertThat(foundMovie.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(201));
        assertThat(foundMovie.getBody().duration()).isEqualTo(20);
    }

    @Test
    void updateMovie_validRequestBody_returnsResponseWithMovie(){
        UUID id = UUID.randomUUID();
        Mockito.when(movieFacade.updateMovie(id, movieCreateDto)).thenReturn(movieDetailedViewDto);

        ResponseEntity<MovieDetailedViewDto> movie = movieRestController.updateMovie(id, movieCreateDto);

        assertThat(movie.getBody()).isNotNull();
        assertThat(movie.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(movie.getBody().duration()).isEqualTo(20);
    }

    @Test
    void deleteMovie_noContentResponse_callsDeleteRatingOnFacade(){
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(movieFacade).delete(id);

        movieRestController.deleteMovie(id);

        Mockito.verify(movieFacade, Mockito.times(1)).delete(id);
    }

    @Test
    void findImagesForMovie_validRequestBody_returnsImagesForMovie(){
        UUID id = UUID.randomUUID();
        ImageDto imageDto = new ImageDto();
        imageDto.setUrl("random");
        //imageDto.setMovieId(id);
        imageDto.setDescription("test");
        Mockito.when(movieFacade.findImagesForMovie(id)).thenReturn(List.of(imageDto));

        ResponseEntity<List<ImageDto>> foundImagesForMovie = movieRestController.showMovieImages(id);

        assertThat( foundImagesForMovie.getBody()).isNotNull();
        assertThat( foundImagesForMovie.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
    }
}
