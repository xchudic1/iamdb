package cz.muni.fi.iamdb;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = MovieMicroservice.class)
@ActiveProfiles("test")
public class SpringBootIAMDBApplicationTests {

    @Test
    void contextLoads() {
    }

}
