package cz.muni.fi.iamdb.data.repository;

import cz.muni.fi.iamdb.data.model.Image;
import cz.muni.fi.iamdb.data.model.Movie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ExtendWith(SpringExtension.class)
public class ImageRepositoryTest {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Image testImage1;
    private Image testImage2;

    @BeforeEach
    void initData() {
        Movie movie = new Movie();
        movie.setTitle("Test movie");
        movie.setYearOfRelease(2000);
        movie.setDuration(120);
        movie.setDescription("Test description");
        movie = testEntityManager.persistFlushFind(movie);

        testImage1 = new Image();
        testImage1.setUrl("image/jpeg");
        testImage1.setDescription("image1");
        testImage1.setMovie(movie);

        testImage2 = new Image();
        testImage2.setUrl("image/jpeg");
        testImage2.setDescription("image2");
        testImage2.setMovie(movie);

        testImage1 = testEntityManager.persistFlushFind(testImage1);
        testImage2 = testEntityManager.persistFlushFind(testImage2);
    }

    @Test
    void findById_ShouldReturnImage_WhenImageExists() {
        var result = imageRepository.findById(testImage1.getId());
        assertTrue(result.isPresent());
        assertEquals(testImage1.getId(), result.get().getId());
    }

    @Test
    void findAll_ShouldReturnAllImages() {
        var images = imageRepository.findAll();
        assertFalse(images.isEmpty());
        assertTrue(images.size() >= 2); // There might be more images from other tests
    }

    @Test
    void findByMovieId_ShouldReturnImages_WhenImagesExist() {
        var images = imageRepository.findByMovieId(testImage1.getMovie().getId());
        assertFalse(images.isEmpty());
    }

    @Test
    void deleteById_ShouldRemoveImage_WhenImageExists() {
        imageRepository.deleteById(testImage1.getId());
        var result = imageRepository.findById(testImage1.getId());
        assertTrue(result.isEmpty());
    }

    @Test
    void create_ShouldCreateImage_WhenImageDoesNotExist() {
        Image image = new Image();
        image.setUrl("image/jpeg");
        image.setDescription("image3");
        image.setMovie(new Movie());
        image = imageRepository.save(image);
        assertNotNull(image.getId());
    }
    @Test
    void update_ShouldUpdateImage_WhenImageExists() {
        String updatedDescription = "Updated description";
        testImage1.setDescription(updatedDescription);
        Image updated = imageRepository.save(testImage1); // Save to simulate update

        assertEquals(updatedDescription, updated.getDescription());
        assertEquals(updatedDescription, imageRepository.findById(testImage1.getId()).get().getDescription());
    }


}
