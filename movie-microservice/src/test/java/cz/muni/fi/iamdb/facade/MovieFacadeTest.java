package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.*;
import cz.muni.fi.iamdb.data.model.Genre;
import cz.muni.fi.iamdb.data.model.Movie;
import cz.muni.fi.iamdb.mappers.MovieMapper;
import cz.muni.fi.iamdb.service.GenreService;
import cz.muni.fi.iamdb.service.ImageService;
import cz.muni.fi.iamdb.service.MovieService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.ap.internal.util.Collections;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class MovieFacadeTest {

    @Mock
    private MovieService movieService;

    @Mock
    private MovieMapper movieMapper;

    @Mock
    private GenreService genreService;

    @Mock
    private ImageService imageService;
    @InjectMocks
    private MovieFacade movieFacade;

    private MovieCreateDto movieCreateDto;

    private MovieDetailedViewDto movieDetailedViewDto;

    private Movie movie;

    private Genre genre = new Genre();

    @BeforeEach
    void setUp() {
        genre.setName("comedy");
        UUID id = UUID.randomUUID();
        genre.setId(id);

        movieDetailedViewDto = new MovieDetailedViewDto(id, "Title", 1999, new GenreDto[0], new ImageDto[0], 20, "desc");


        movie = new Movie();
        movie.setTitle("Title");
        movie.setYearOfRelease(1999);
        movie.setGenres(Collections.asSet(genre));
        movie.setDuration(20);
        movie.setDescription("desc");

        movieCreateDto = new MovieCreateDto();
        movieCreateDto.setYearOfRelease(1999);
        movieCreateDto.setTitle("Title");
        movieCreateDto.setDuration(20);
        movieCreateDto.setDescription("desc");
    }
    
    @Test
    void createMovie_movieCreated_returnsMovie(){

        Mockito.when(movieMapper.mapToEntity(movieCreateDto)).thenReturn(movie);
        Mockito.when(movieService.createMovie(movie)).thenReturn(movie);
        Mockito.when(movieMapper.mapToDetailViewDto(movie)).thenReturn(movieDetailedViewDto);

        MovieDetailedViewDto createdMovie = movieFacade.createMovie(movieCreateDto);

        assertThat(createdMovie).isEqualTo(movieDetailedViewDto);
    }
    @Test
    void updateMovie_movieUpdated_returnsMovie(){
        UUID id = UUID.randomUUID();
        Mockito.when(movieMapper.mapToEntity(movieCreateDto)).thenReturn(movie);
        Mockito.when(movieService.updateMovie(id, movie)).thenReturn(movie);
        Mockito.when(movieMapper.mapToDetailViewDto(movie)).thenReturn(movieDetailedViewDto);

        MovieDetailedViewDto updatedMovie = movieFacade.updateMovie(id, movieCreateDto);

        assertThat(updatedMovie).isEqualTo(movieDetailedViewDto);
    }
    @Test
    void deleteMovie_movieDeleted_callsOnDeleteOnService(){
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(movieService).deleteMovie(id);

        movieFacade.delete(id);

        Mockito.verify(movieService, Mockito.times(1)).deleteMovie(id);
    }

    @Test
    void findById_movieFound_returnsMovie(){
        UUID id = UUID.randomUUID();
        Mockito.when(movieService.findById(id)).thenReturn(movie);
        Mockito.when(movieMapper.mapToDetailViewDto(movie)).thenReturn(movieDetailedViewDto);


        MovieDetailedViewDto foundMovie = movieFacade.findById(id);

        assertThat(foundMovie).isEqualTo(movieDetailedViewDto);
    }

    @Test
    void findAll_moviesFound_returnsMovies(){
        UUID id = UUID.randomUUID();
        Mockito.when(movieService.findAll(Optional.empty())).thenReturn(List.of(movie));
        Mockito.when(movieMapper.mapToList(List.of(movie))).thenReturn(List.of(new MovieBasicViewDto(id, "Title", 20, 1999, new GenreDto[1])));

        List<MovieBasicViewDto> foundMovies = movieFacade.findAll(Optional.empty());

        assertThat(foundMovies).isNotEmpty();
    }

}
