package cz.muni.fi.iamdb.data.repository;


import cz.muni.fi.iamdb.data.model.Movie;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@ExtendWith(SpringExtension.class)
public class MovieRepositoryTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Movie testMovie1;

    private Movie testMovie2;

    @BeforeEach
    void initData() {
        testMovie1 = new Movie();
        testMovie1.setTitle("movie1");
        testMovie1.setYearOfRelease(2001);
        testMovie1.setGenres(Collections.emptySet());
        testMovie1.setDuration(53);
        testMovie1.setDescription("Movie description ");

        testMovie2 = new Movie();
        testMovie2.setTitle("movie2");
        testMovie2.setYearOfRelease(2001);
        testMovie2.setGenres(Collections.emptySet());
        testMovie2.setDuration(53);
        testMovie2.setDescription("Movie description");


        testMovie1 = testEntityManager.persistFlushFind(testMovie1);
        testMovie2 = testEntityManager.persistFlushFind(testMovie2);
    }

    @AfterEach
    void tearDown() {
        testEntityManager.clear();
    }


    @Test
    void findById_ShouldReturnMovie_WhenMovieExists() {
        Optional<Movie> result = movieRepository.findById(testMovie1.getId());
        assertTrue(result.isPresent());
        assertEquals(testMovie1.getId(), result.get().getId());
    }
    @Test
    void findAll_ShouldReturnAllMovies() {
        var movies = movieRepository.findAll();
        assertFalse(movies.isEmpty());
        assertTrue(movies.size() >= 2);
    }

    @Test
    void deleteById_ShouldDeleteMovie_WhenMovieExists() {
        movieRepository.deleteById(testMovie2.getId());
        var result = movieRepository.findById(testMovie2.getId());
        assertTrue(result.isEmpty());
    }

    @Test
    void create_ShouldCreateMovie_WhenMovieDoesNotExist() {
        Movie newMovie = new Movie();
        newMovie.setTitle("movie3");
        newMovie.setYearOfRelease(2001);
        newMovie.setGenres(Collections.emptySet());
        newMovie.setDuration(53);
        newMovie.setDescription("Movie description");

        Movie created = movieRepository.save(newMovie);
        assertNotNull(created.getId());
        assertEquals(newMovie.getTitle(), created.getTitle());
        assertEquals(newMovie.getYearOfRelease(), created.getYearOfRelease());
        assertEquals(newMovie.getGenres(), created.getGenres());
        assertEquals(newMovie.getDuration(), created.getDuration());
        assertEquals(newMovie.getDescription(), created.getDescription());
    }

    @Test
    void update_ShouldUpdateMovie_WhenMovieExists() {
        String updatedDescription = "Updated description";
        testMovie1.setDescription(updatedDescription);
        Movie updated = movieRepository.save(testMovie1);

        assertEquals(updatedDescription, updated.getDescription());
        assertTrue(movieRepository.findById(testMovie1.getId()).isPresent());
        assertEquals(updatedDescription, movieRepository.findById(testMovie1.getId()).get().getDescription());
    }


}
