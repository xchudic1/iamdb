package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Movie;
import cz.muni.fi.iamdb.data.repository.MovieRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class MovieServiceTest {

    @InjectMocks
    private MovieService movieService;

    @Mock
    private MovieRepository movieRepository;

    @Test
    void create_movieCreated_returnsMovie() {
        // Arrange
        Movie newMovie = new Movie();
        newMovie.setTitle("movie2");
        newMovie.setYearOfRelease(2001);
        newMovie.setGenres(Collections.emptySet());
        newMovie.setDuration(53);
        newMovie.setDescription("Movie description");

        Mockito.when(movieRepository.save(newMovie)).thenReturn(newMovie);

        // Act
        Movie createdMovie = movieService.createMovie(newMovie);

        // Assert
        assertThat(createdMovie).isEqualTo(newMovie);
    }

    @Test
    void findById_movieFound_returnsMovie() {
        // Arrange
        UUID id = UUID.randomUUID();
        Movie movie = new Movie();
        movie.setTitle("movie2");
        movie.setYearOfRelease(2001);
        movie.setGenres(Collections.emptySet());
        movie.setDuration(53);
        movie.setDescription("Movie description");

        Mockito.when(movieRepository.findById(id)).thenReturn(java.util.Optional.of(movie));

        // Act
        Movie result = movieService.findById(id);

        // Assert
        assertThat(result).isEqualTo(movie);
    }

    @Test
    void findById_movieNotFound_throwsResourceNotFoundException() {
        // Arrange
        UUID id = UUID.randomUUID();
        Mockito.when(movieRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act
        assertThrows(ResourceNotFoundException.class, () -> movieService.findById(id));
    }

    @Test
    void findAll_moviesFound_returnsMovies() {
        // Arrange
        Movie movie1 = new Movie();
        movie1.setTitle("movie1");
        movie1.setYearOfRelease(2001);
        movie1.setGenres(Collections.emptySet());
        movie1.setDuration(53);
        movie1.setDescription("Movie description");

        Movie movie2 = new Movie();
        movie2.setTitle("movie2");
        movie2.setYearOfRelease(2001);
        movie2.setGenres(Collections.emptySet());
        movie2.setDuration(53);
        movie2.setDescription("Movie description");

        Mockito.when(movieRepository.findAllByCriteria(Optional.empty())).thenReturn(List.of(movie1, movie2));

        // Act
        List<Movie> result = movieService.findAll(Optional.empty());

        // Assert
        assertThat(result).containsExactlyInAnyOrder(movie1, movie2);
    }

    @Test
    void updateMovie_movieUpdated_returnsMovie() {
        // Arrange
        UUID id = UUID.randomUUID();
        Movie movie = new Movie();
        movie.setTitle("movie2");
        movie.setYearOfRelease(2001);
        movie.setGenres(Collections.emptySet());
        movie.setDuration(53);
        movie.setDescription("Movie description");

        Mockito.when(movieRepository.existsById(id)).thenReturn(true);
        Mockito.when(movieRepository.save(movie)).thenReturn(movie);
        Mockito.when(movieRepository.findById(id)).thenReturn(Optional.of(movie));

        // Act
        Movie updatedMovie = movieService.updateMovie(id, movie);

        // Assert
        assertThat(updatedMovie).isEqualTo(movie);
    }

    @Test
    void updateMovie_movieNotFound_throwsResourceNotFoundException() {
        // Arrange
        UUID id = UUID.randomUUID();
        Movie movie = new Movie();
        movie.setTitle("movie2");
        movie.setYearOfRelease(2001);
        movie.setGenres(Collections.emptySet());
        movie.setDuration(53);
        movie.setDescription("Movie description");

        Mockito.when(movieRepository.existsById(id)).thenReturn(false);

        // Act
        assertThrows(ResourceNotFoundException.class, () -> movieService.updateMovie(id, movie));
    }


}
