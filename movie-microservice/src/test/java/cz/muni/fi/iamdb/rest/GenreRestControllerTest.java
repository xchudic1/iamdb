package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.GenreDto;
import cz.muni.fi.iamdb.facade.GenreFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class GenreRestControllerTest {


    @Mock
    private GenreFacade genreFacade;
    

    @InjectMocks
    private GenreRestController genreRestController;

    private GenreDto genreDto;

    @BeforeEach
    void setUp(){
        genreDto = new GenreDto();
        genreDto.setName("comedy");
    }

    @Test
    void  findById_validRequestBody_returnsResponseWithGenre(){
        UUID id = UUID.randomUUID();
        Mockito.when(genreFacade.findById(id)).thenReturn(genreDto);

        ResponseEntity<GenreDto> foundGenre = genreRestController.findById(id);

        assertThat(foundGenre.getBody()).isNotNull();
        assertThat(foundGenre.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(foundGenre.getBody().getName()).isEqualTo("comedy");
    }

    @Test
    void  findAll_validRequestBody_returnsResponseWithGenres(){
        Mockito.when(genreFacade.findAll()).thenReturn(List.of(genreDto));

        ResponseEntity<List<GenreDto>> foundGenres = genreRestController.findAll();

        assertThat(foundGenres.getBody()).isNotNull();
        assertThat(foundGenres.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
    }

    @Test
    void createGenre_validRequestBody_returnsResponseWithGenre(){
        Mockito.when(genreFacade.createGenre(genreDto)).thenReturn(genreDto);

        ResponseEntity<GenreDto> foundGenre = genreRestController.createGenre(genreDto);

        assertThat(foundGenre.getBody()).isNotNull();
        assertThat(foundGenre.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(201));
        assertThat(foundGenre.getBody().getName()).isEqualTo("comedy");
    }

    @Test
    void updateGenre_validRequestBody_returnsResponseWithGenre(){
        UUID id = UUID.randomUUID();
        Mockito.when(genreFacade.updateGenre(id, genreDto)).thenReturn(genreDto);

        ResponseEntity<GenreDto> Genre = genreRestController.updateGenre(id, genreDto);

        assertThat(Genre.getBody()).isNotNull();
        assertThat(Genre.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(Genre.getBody().getName()).isEqualTo("comedy");
    }

    @Test
    void deleteGenre_noContentResponse_callsDeleteRatingOnFacade(){
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(genreFacade).deleteGenre(id);

        genreRestController.deleteGenre(id);

        Mockito.verify(genreFacade, Mockito.times(1)).deleteGenre(id);
    }

}
