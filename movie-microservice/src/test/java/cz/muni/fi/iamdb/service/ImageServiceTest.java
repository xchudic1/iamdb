package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Image;
import cz.muni.fi.iamdb.data.model.Movie;
import cz.muni.fi.iamdb.data.repository.ImageRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
@ExtendWith(MockitoExtension.class)
public class ImageServiceTest {

    @InjectMocks
    private ImageService imageService;

    @Mock
    private ImageRepository imageRepository;

    @Test
    void create_imageCreated_returnsImage() {
        // Arrange
        Image newImage = new Image();
        newImage.setUrl("image/jpeg");
        newImage.setDescription("image1");
        newImage.setMovie(new Movie());

        Mockito.when(imageRepository.save(newImage)).thenReturn(newImage);

        // Act
        Image createdImage = imageService.createImage(newImage);

        // Assert
        assertThat(createdImage).isEqualTo(newImage);
    }



    @Test
    void findById_imageNotFound_throwsResourceNotFoundException() {
        // Arrange
        UUID id = UUID.randomUUID();
        Mockito.when(imageRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act
        assertThrows(ResourceNotFoundException.class, () -> imageService.findById(id));
    }


    @Test
    void findAll_imagesFound_returnsImages() {
        // Arrange
        Image mock_image1 = new Image();
        mock_image1.setUrl("image/jpeg");
        mock_image1.setDescription("image1");
        mock_image1.setMovie(new Movie());

        Image mock_image2 = new Image();
        mock_image2.setUrl("image/jpeg");
        mock_image2.setDescription("image2");
        mock_image2.setMovie(new Movie());

        Mockito.when(imageRepository.findAll()).thenReturn(List.of(mock_image1, mock_image2));

        // Act
        List<Image> result = imageService.findAll();

        // Assert
        assertThat(result).containsExactly(mock_image1, mock_image2);
    }

    @Test
    void findAll_noImages_returnsEmptyList() {
        // Arrange
        Mockito.when(imageRepository.findAll()).thenReturn(List.of());

        // Act
        List<Image> result = imageService.findAll();

        // Assert
        assertThat(result).isEmpty();
    }

}
