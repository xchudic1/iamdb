package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.MovieCreateDto;
import cz.muni.fi.iamdb.util.ObjectConverter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class MovieRestControllerIT {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void testCreateAndFindById() throws Exception {
        MovieCreateDto movieCreateDto = new MovieCreateDto();
        movieCreateDto.setTitle("title");
        movieCreateDto.setDuration(20);
        movieCreateDto.setDescription("description");
        movieCreateDto.setYearOfRelease(2000);

        String inputJson = ObjectConverter.convertObjectToJson(movieCreateDto);
        MvcResult result = mockMvc.perform(post("/movies")
                        .contentType("application/json")
                        .content(inputJson))
                .andExpect(status().isCreated())
                .andReturn();

        String responseJson = result.getResponse().getContentAsString(StandardCharsets.UTF_8);
        MovieCreateDto response = ObjectConverter.convertJsonToObject(responseJson, MovieCreateDto.class);
        System.out.println(responseJson);
        System.out.println(response);
        assertThat(response.getTitle()).isEqualTo(movieCreateDto.getTitle());
        assertThat(response.getDuration()).isEqualTo(movieCreateDto.getDuration());
        assertThat(response.getDescription()).isEqualTo(movieCreateDto.getDescription());
        assertThat(response.getYearOfRelease()).isEqualTo(movieCreateDto.getYearOfRelease());

    }

    @Test
    void testFindAll() throws Exception {
        mockMvc.perform(get("/movies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    void testCreateInvalid() throws Exception {
        String mockMovie = """
                {
                  "title": "",
                  "duration": 0,
                  "description": "description",
                  "year_of_release": 1888,
                  "genre_ids": [],
                  "image_ids": []
                }""";
        mockMvc.perform(post("/movies")
                        .contentType("application/json")
                        .content(mockMovie))
                .andExpect(status().isBadRequest());
    }

}
