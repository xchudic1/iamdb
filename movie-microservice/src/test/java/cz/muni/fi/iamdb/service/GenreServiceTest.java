package cz.muni.fi.iamdb.service;

import cz.muni.fi.iamdb.data.model.Genre;
import cz.muni.fi.iamdb.data.repository.GenreRepository;
import cz.muni.fi.iamdb.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(MockitoExtension.class)
public class GenreServiceTest {

    @InjectMocks
    private GenreService genreService;

    @Mock
    private GenreRepository genreRepository;

    @Test
    void create_whenCalled_mapsAndReturnsGenre() {
        // Arrange
        UUID id = UUID.randomUUID();
        Genre genre = new Genre();
        genre.setName("Action");
        genre.setId(id);

        Mockito.when(genreRepository.save(genre)).thenReturn(genre);

        // Act
        Genre result = genreService.createGenre(genre);

        // Assert
        assertThat(result).isEqualTo(genre);
    }

    @Test
    void findById_whenFound_returnsGenre() {
        // Arrange
        UUID id = UUID.randomUUID();
        Genre genre = new Genre();
        genre.setName("Action");
        genre.setId(id);

        Mockito.when(genreRepository.findById(id)).thenReturn(java.util.Optional.of(genre));

        // Act
        Genre result = genreService.findById(id);

        // Assert
        assertThat(result).isEqualTo(genre);
    }

    @Test
    void findById_whenNotFound_throwsResourceNotFoundException() {
        // Arrange
        UUID id = UUID.randomUUID();
        Mockito.when(genreRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Assert
        assertThrows(ResourceNotFoundException.class, () -> genreService.findById(id));

    }


    @Test
    void findAll_whenCalled_returnsGenres() {
        // Arrange
        UUID id1 = UUID.randomUUID();
        Genre genre1 = new Genre();
        genre1.setName("Action");
        genre1.setId(id1);
        UUID id2 = UUID.randomUUID();
        Genre genre2 = new Genre();
        genre2.setName("Comedy");
        genre2.setId(id2);

        Mockito.when(genreRepository.findAll()).thenReturn(java.util.List.of(genre1, genre2));

        // Act
        java.util.List<Genre> result = genreService.findAll();

        // Assert
        assertThat(result).containsExactly(genre1, genre2);
    }

    @Test
    void findAll_whenNoGenres_returnsEmptyList() {
        // Arrange
        Mockito.when(genreRepository.findAll()).thenReturn(java.util.List.of());

        // Act
        java.util.List<Genre> result = genreService.findAll();

        // Assert
        assertThat(result).isEmpty();
    }


}
