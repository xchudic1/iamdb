package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.ImageDto;
import cz.muni.fi.iamdb.data.model.Genre;
import cz.muni.fi.iamdb.data.model.Image;
import cz.muni.fi.iamdb.data.model.Movie;
import cz.muni.fi.iamdb.mappers.ImageMapper;
import cz.muni.fi.iamdb.service.ImageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ImageFacadeTest {

    @Mock
    private ImageService imageService;

    @Mock
    private ImageMapper imageMapper;

    @InjectMocks
    private ImageFacade imageFacade;

    private ImageDto imageDto;
    
    private Image image = new Image();
    @BeforeEach
    void setUp() {
        image.setMovie(new Movie());
        image.setDescription("random");
        image.setUrl("test");

        imageDto = new ImageDto();
        imageDto.setUrl("random");
        imageDto.setDescription("test");

    }

    @Test
    void createImage_imageCreated_returnsImage(){
        Mockito.when(imageMapper.mapToEntity(imageDto)).thenReturn(image);
        Mockito.when(imageService.createImage(image)).thenReturn(image);
        Mockito.when(imageMapper.mapToDto(image)).thenReturn(imageDto);

        ImageDto createdImage = imageFacade.createImage(imageDto);

        assertThat(createdImage).isEqualTo(imageDto);
    }
    @Test
    void updateImage_imageUpdated_returnsImage(){
        UUID id = UUID.randomUUID();
        Mockito.when(imageMapper.mapToEntity(imageDto)).thenReturn(image);
        Mockito.when(imageService.updateImage(id, image)).thenReturn(image);
        Mockito.when(imageMapper.mapToDto(image)).thenReturn(imageDto);

        ImageDto updatedImage = imageFacade.updateImage(id, imageDto);

        assertThat(updatedImage).isEqualTo(imageDto);
    }
    @Test
    void deleteImage_imageDeleted_callsOnDeleteOnService(){
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(imageService).deleteImage(id);

        imageFacade.deleteImage(id);

        Mockito.verify(imageService, Mockito.times(1)).deleteImage(id);
    }

    @Test
    void findById_imageFound_returnsImage(){
        UUID id = UUID.randomUUID();
        Genre genre = new Genre();
        genre.setName("Comedy");
        genre.setId(id);
        Mockito.when(imageService.findById(id)).thenReturn(image);
        Mockito.when(imageMapper.mapToDto(image)).thenReturn(imageDto);

        ImageDto foundImage = imageFacade.findById(id);

        assertThat(foundImage).isEqualTo(imageDto);
    }

    @Test
    void findAll_imagesFound_returnsImages(){


        Mockito.when(imageService.findAll()).thenReturn(List.of(image));
        Mockito.when(imageMapper.mapToList(List.of(image))).thenReturn(List.of(imageDto));

        List<ImageDto> foundImages = imageFacade.findAll();

        assertThat(foundImages).isNotEmpty();
    }

}
