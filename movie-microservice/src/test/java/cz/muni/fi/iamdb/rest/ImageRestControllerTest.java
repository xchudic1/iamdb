package cz.muni.fi.iamdb.rest;

import cz.muni.fi.iamdb.api.ImageDto;
import cz.muni.fi.iamdb.facade.ImageFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
final class ImageRestControllerTest {
    @Mock
    private ImageFacade imageFacade;

    @InjectMocks
    private ImageRestController imageRestController;

    private ImageDto imageDto;

    @BeforeEach
    void setUp(){
        imageDto = new ImageDto();
        imageDto.setDescription("test");
        //imageDto.setMovieId(id);
        imageDto.setUrl("random");
    }

    @Test
    void  findById_validRequestBody_returnsResponseWithImage(){
        UUID id = UUID.randomUUID();
        Mockito.when(imageFacade.findById(id)).thenReturn(imageDto);

        ResponseEntity<ImageDto> foundImage = imageRestController.findById(id);

        assertThat(foundImage.getBody()).isNotNull();
        assertThat(foundImage.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        //assertThat(foundImage.getBody().getMovieId()).isEqualTo(id);
    }

    @Test
    void  findAll_validRequestBody_returnsResponseWithImages(){
        Mockito.when(imageFacade.findAll()).thenReturn(List.of(imageDto));

        ResponseEntity<List<ImageDto>> foundImages = imageRestController.findAll();

        assertThat(foundImages.getBody()).isNotNull();
        assertThat(foundImages.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
    }

    @Test
    void createImage_validRequestBody_returnsResponseWithImage(){
        Mockito.when(imageFacade.createImage(imageDto)).thenReturn(imageDto);

        ResponseEntity<ImageDto> foundImage = imageRestController.createImage(imageDto);

        assertThat(foundImage.getBody()).isNotNull();
        assertThat(foundImage.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(201));
        //assertThat(foundImage.getBody().getMovieId()).isEqualTo(id);
    }

    @Test
    void updateImage_validRequestBody_returnsResponseWithImage(){
        UUID id = UUID.randomUUID();
        Mockito.when(imageFacade.updateImage(id, imageDto)).thenReturn(imageDto);

        ResponseEntity<ImageDto> Image = imageRestController.updateImage(id, imageDto);

        assertThat(Image.getBody()).isNotNull();
        assertThat(Image.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        //assertThat(Image.getBody().getMovieId()).isEqualTo(id);
    }

    @Test
    void deleteImage_noContentResponse_callsDeleteRatingOnFacade(){
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(imageFacade).deleteImage(id);

        imageRestController.deleteImage(id);

        Mockito.verify(imageFacade, Mockito.times(1)).deleteImage(id);
    }
}
