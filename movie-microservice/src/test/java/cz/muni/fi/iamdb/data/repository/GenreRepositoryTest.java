package cz.muni.fi.iamdb.data.repository;

import cz.muni.fi.iamdb.data.model.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ExtendWith(SpringExtension.class)
public class GenreRepositoryTest {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    private Genre testGenre1;
    private Genre testGenre2;
    @BeforeEach
    void initData() {
        testGenre1 = new Genre();
        testGenre1.setName("Action");
        testGenre2 = new Genre();
        testGenre2.setName("Comedy");

        testGenre1 = testEntityManager.persistFlushFind(testGenre1);
        testGenre2 = testEntityManager.persistFlushFind(testGenre2);
    }

    @Test
    void findById_ShouldReturnGenre_WhenGenreExists() {
        var result = genreRepository.findById(testGenre1.getId());
        assertTrue(result.isPresent());
        assertEquals(testGenre1.getId(), result.get().getId());
    }

    @Test
    void findAll_ShouldReturnAllGenres() {
        var genres = genreRepository.findAll();
        assertFalse(genres.isEmpty());
        assertTrue(genres.size() >= 2);
    }


    @Test
    void deleteById_ShouldDeleteGenre_WhenGenreExists() {
        genreRepository.deleteById(testGenre2.getId());
        var result = genreRepository.findById(testGenre2.getId());
        assertTrue(result.isEmpty());
    }
}
