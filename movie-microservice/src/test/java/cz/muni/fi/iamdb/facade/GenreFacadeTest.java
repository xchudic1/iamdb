package cz.muni.fi.iamdb.facade;

import cz.muni.fi.iamdb.api.GenreDto;
import cz.muni.fi.iamdb.data.model.Genre;
import cz.muni.fi.iamdb.mappers.GenreMapper;
import cz.muni.fi.iamdb.service.GenreService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class GenreFacadeTest {

    @Mock
    private GenreService genreService;

    @Mock
    private GenreMapper genreMapper;

    @InjectMocks
    private GenreFacade genreFacade;

    private GenreDto genreDto;

    private Genre genre = new Genre();
    @BeforeEach
    void setUp() {
        genre.setName("test");

        genreDto = new GenreDto();
        genreDto.setName("test");

    }

    @Test
    void createGenre_genreCreated_returnsGenre(){
        Mockito.when(genreMapper.mapToEntity(genreDto)).thenReturn(genre);
        Mockito.when(genreService.createGenre(genre)).thenReturn(genre);
        Mockito.when(genreMapper.mapToDto(genre)).thenReturn(genreDto);

        GenreDto createdGenre = genreFacade.createGenre(genreDto);

        assertThat(createdGenre).isEqualTo(genreDto);
    }
    @Test
    void updateGenre_genreUpdated_returnsGenre(){
        UUID id = UUID.randomUUID();
        genre.setId(id);
        Mockito.when(genreMapper.mapToEntity(genreDto)).thenReturn(genre);
        Mockito.when(genreService.updateGenre(genre.getId(), genre)).thenReturn(genre);
        Mockito.when(genreMapper.mapToDto(genre)).thenReturn(genreDto);

        GenreDto updatedGenre = genreFacade.updateGenre(genre.getId(), genreDto);

        assertThat(updatedGenre).isEqualTo(genreDto);
    }
    @Test
    void deleteGenre_genreDeleted_callsOnDeleteOnService(){
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(genreService).deleteGenre(id);

        genreFacade.deleteGenre(id);

        Mockito.verify(genreService, Mockito.times(1)).deleteGenre(id);
    }

    @Test
    void findById_genreFound_returnsGenre(){
        UUID id = UUID.randomUUID();
        Mockito.when(genreService.findById(id)).thenReturn(genre);
        Mockito.when(genreMapper.mapToDto(genre)).thenReturn(genreDto);

        GenreDto foundGenre = genreFacade.findById(id);

        assertThat(foundGenre).isEqualTo(genreDto);
    }

    @Test
    void findAll_genresFound_returnsGenres(){


        Mockito.when(genreService.findAll()).thenReturn(List.of(genre));
        Mockito.when(genreMapper.mapToList(List.of(genre))).thenReturn(List.of(genreDto));

        List<GenreDto> foundGenres = genreFacade.findAll();

        assertThat(foundGenres).isNotEmpty();
    }


}
